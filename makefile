OCAML_DEPS := src/ocaml/ast.ml src/ocaml/dune src/ocaml/dune-project \
	src/ocaml/memory.ml src/ocaml/memory.mli src/ocaml/parser.ml \
	src/ocaml/primitives.ml src/ocaml/runtime.ml src/ocaml/runtime.mli \
	src/ocaml/topple.ml

C_SRC := src/c/control.c src/c/memory.c src/c/primitives.c src/c/topple.c
C_DEPS := ${C_SRC} src/c/control.h src/c/memory.h src/c/topple.h

ASM_SRC := src/asm/topple.s src/asm/syscall/libc.c
ASM_DEPS := ${ASM_SRC} src/asm/boot.tpl src/asm/runtime.tpl src/asm/tpl-asm.sh


UNAME := $(shell uname)

ifeq "${UNAME}" "Darwin"
	ASM_SRC += src/asm/syscall/darwin.s
else
	ASM_SRC += src/asm/syscall/linux.s
endif


all: build/tpl-ocaml build/tpl-c build/tpl-asm
clean:
	rm -rf build
check: build/tpl-ocaml build/tpl-c build/tpl-asm
	./test/run_tests.py

build/tpl-ocaml: ${OCAML_DEPS}
	mkdir -p build
	dune build --build-dir="${PWD}/build/ocaml" --root=src/ocaml topple.exe
	cp build/ocaml/default/topple.exe build/tpl-ocaml

build/tpl-c: ${C_DEPS}
	mkdir -p build
	cc -o build/tpl-c -Os ${C_SRC}

build/tpl-asm: ${ASM_DEPS}
	mkdir -p build/asm
	cc -o build/asm/tpl-asm -Os ${ASM_SRC}
	cat src/asm/boot.tpl src/asm/runtime.tpl > build/asm/lib.tpl
	cp src/asm/tpl-asm.sh build/tpl-asm

.PHONY: all clean check
