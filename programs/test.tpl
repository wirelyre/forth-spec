: file-path
  bnew
    116 over b% 101 over b%
    115 over b% 116 over b%
    46  over b% 116 over b%
    112 over b% 108 over b%
;

: output-contents
  0
  begin 2dup swap blen < while
    2dup b@ putc
    1 +
  repeat
  drop drop
;

: main
  file-path fread output-contents
;

main
