rnew constant r

bnew                  constant filename
filename R cursor.new constant file-cur

: read-file \ cur --
  filename bclear
  filename over cursor.copy
  filename fread

  dup if else
    drop
    "Error: couldn't open file \"" cursor.print "\"\n"
    1 fail
  then

  file-cur swap cursor.zap
  drop
;

: handle-files
  args.files
  begin dup @ dup while
    read-file
    file-cur parse.file drop
  1 p+ repeat
  drop
;

: print-generated
  begin dup cursor.peek dup EOF <>
  while putc dup cursor.consume repeat drop
;

object.section.main
  232 b%8 9 b%32        \ call +0x9
  137 b%8 199 b%8       \ mov edi, eax
  184 b%8 33554433 b%32 \ mov eax, 0x02000001 (exit)
  15  b%8 5 b%8         \ syscall
  184 b%8 4 b%32        \ mov eax, 4
  131 b%8 192 b%8 7 b%8 \ add eax, 4
  195 b%8               \ ret
drop

mach-o.end
mach-o.layout
mach-o.bytes

filename bclear
filename args.output @ cursor.copy

object.file filename fwrite
