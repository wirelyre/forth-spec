R dict.new   constant tok.dict



\ $ python3 programs/compiler/gen-tokens.py

: tok.EOF     EOF ;
: tok.WORD      0 ;
: tok.STRING    1 ;

: tok.+         2 ; : tok.-         3 ; : tok.*         4 ; : tok./         5 ;
: tok.=         6 ; : tok.<>        7 ; : tok.<         8 ; : tok.>         9 ;
: tok.or       10 ; : tok.xor      11 ; : tok.and      12 ; : tok.not      13 ;
: tok.<<       14 ; : tok.>>       15 ; : tok.@        16 ; : tok.!        17 ;
: tok.putc     18 ; : tok.fail     19 ; : tok.drop     20 ; : tok.nip      21 ;
: tok.dup      22 ; : tok.over     23 ; : tok.tuck     24 ; : tok.swap     25 ;
: tok.rot      26 ; : tok.-rot     27 ; : tok.pick     28 ; : tok.:        29 ;
: tok.;        30 ; : tok.:d       31 ; : tok.defer    32 ; : tok.exit     33 ;
: tok.if       34 ; : tok.else     35 ; : tok.then     36 ; : tok.begin    37 ;
: tok.while    38 ; : tok.repeat   39 ; : tok.constant 40 ; : tok.variable 41 ;

0 tok.+ 0 43 tok.- 0 45 tok.* 0 42 tok./ 0 47 tok.= 0 61 tok.<> 0 62 60 tok.< 0
60 tok.> 0 62 tok.or 0 114 111 tok.xor 0 114 111 120 tok.and 0 100 110 97
tok.not 0 116 111 110 tok.<< 0 60 60 tok.>> 0 62 62 tok.@ 0 64 tok.! 0 33
tok.putc 0 99 116 117 112 tok.fail 0 108 105 97 102 tok.drop 0 112 111 114 100
tok.nip 0 112 105 110 tok.dup 0 112 117 100 tok.over 0 114 101 118 111 tok.tuck
0 107 99 117 116 tok.swap 0 112 97 119 115 tok.rot 0 116 111 114 tok.-rot 0 116
111 114 45 tok.pick 0 107 99 105 112 tok.: 0 58 tok.; 0 59 tok.:d 0 100 58
tok.defer 0 114 101 102 101 100 tok.exit 0 116 105 120 101 tok.if 0 102 105
tok.else 0 101 115 108 101 tok.then 0 110 101 104 116 tok.begin 0 110 105 103
101 98 tok.while 0 101 108 105 104 119 tok.repeat 0 116 97 101 112 101 114
tok.constant 0 116 110 97 116 115 110 111 99 tok.variable 0 101 108 98 97 105
114 97 118

tok.dict dict.fill



: tok.is-ws? dup 32 = swap 10 = or ; \ ' ' or '\n'
: tok.is-print? dup 32 > swap dup 127 < swap dup 92 <> swap 34 <> and and and ;

: tok.skip-ws \ cursor --
  begin 1 while
    dup cursor.peek
    dup EOF = if drop cursor.skip exit then

    dup tok.is-ws? if drop dup cursor.consume
    else dup 92 =  if drop dup cursor.consume
      begin dup cursor.peek dup EOF <> swap 10 <> and
      while dup cursor.consume repeat
    else drop cursor.skip exit then then
  repeat
;

: tok.next \ cursor -- cursor token-type
  dup tok.skip-ws

  dup cursor.peek
  dup EOF = if drop tok.EOF exit else over cursor.consume then

  dup tok.is-print? if drop
    begin dup cursor.peek tok.is-print?
    while dup cursor.consume repeat
    dup tok.dict dict.search
    ?dup if @ else tok.WORD then

  else dup 34 = if drop
    begin dup cursor.peek dup 34 <> while
      dup EOF = if "unterminated string\n" 1 fail then
      92 = if dup cursor.consume
        dup cursor.peek EOF = if "unterminated string\n" 1 fail then
        dup cursor.consume
      else dup cursor.consume then
    repeat drop dup cursor.consume
    tok.STRING

  else "illegal character\n" 1 fail then then
;
