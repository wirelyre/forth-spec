bnew constant code.text

: code.emit begin dup while code.text b% repeat drop ;
: code.space 0 32 code.emit ;

: code.newdef \ cursor *data -- cursor
  drop
  0 32 58 code.emit
  code.text over cursor.copy code.space
;

: code.enddef 0 32 59 code.emit ;

: code.defer-def
  drop
  0 32 100 58 code.emit
  code.text over cursor.copy code.space
;

: code.defer
  drop
  0 32 114 101 102 101 100 code.emit
  code.text over cursor.copy code.space
;

: code.def-constant
  drop
  0 32 116 110 97 116 115 110 111 99 code.emit
  code.text over cursor.copy code.space
;
: code.def-variable
  drop
  0 32 101 108 98 97 105 114 97 118 code.emit
  code.text over cursor.copy code.space
;



: code.regular-word drop code.text over cursor.copy code.space ;
: code.deferred-word     code.text over cursor.copy code.space ;

: code.number
  dup 0 = if drop 0 32 48 code.emit exit then

  10 swap
  begin dup 0 > while 10 %/ repeat drop
  begin dup 10 = not while 48 + code.text b% repeat
  drop code.space
;

: code.string code.text over cursor.copy code.space ;

: code.emit-exit   code.text over cursor.copy code.space ;

: code.emit-if     code.text over cursor.copy code.space ;
: code.emit-else   code.text over cursor.copy code.space ;
: code.emit-then   code.text over cursor.copy code.space ;

: code.emit-begin  code.text over cursor.copy code.space ;
: code.emit-while  code.text over cursor.copy code.space ;
: code.emit-repeat code.text over cursor.copy code.space ;

: code.emit-drop   code.text over cursor.copy code.space ;
: code.emit-nip    code.text over cursor.copy code.space ;
: code.emit-dup    code.text over cursor.copy code.space ;
: code.emit-over   code.text over cursor.copy code.space ;
: code.emit-tuck   code.text over cursor.copy code.space ;
: code.emit-swap   code.text over cursor.copy code.space ;
: code.emit-rot    code.text over cursor.copy code.space ;
: code.emit--rot   code.text over cursor.copy code.space ;
: code.emit-pick   code.text over cursor.copy code.space ;

: code.emit-+      code.text over cursor.copy code.space ;
: code.emit--      code.text over cursor.copy code.space ;
: code.emit-*      code.text over cursor.copy code.space ;
: code.emit-/      code.text over cursor.copy code.space ;
: code.emit-=      code.text over cursor.copy code.space ;
: code.emit-<>     code.text over cursor.copy code.space ;
: code.emit-<      code.text over cursor.copy code.space ;
: code.emit->      code.text over cursor.copy code.space ;
: code.emit-or     code.text over cursor.copy code.space ;
: code.emit-xor    code.text over cursor.copy code.space ;
: code.emit-and    code.text over cursor.copy code.space ;
: code.emit-not    code.text over cursor.copy code.space ;
: code.emit-<<     code.text over cursor.copy code.space ;
: code.emit->>     code.text over cursor.copy code.space ;
: code.emit-@      code.text over cursor.copy code.space ;
: code.emit-!      code.text over cursor.copy code.space ;
: code.emit-putc   code.text over cursor.copy code.space ;
: code.emit-fail   code.text over cursor.copy code.space ;
