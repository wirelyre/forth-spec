#!/usr/bin/env python3


# fmt: off
tokens = ["+", "-", "*", "/", "=", "<>", "<", ">", "or", "xor", "and", "not",
          "<<", ">>", "@", "!", "putc", "fail", "drop", "nip", "dup", "over",
          "tuck", "swap", "rot", "-rot", "pick", ":", ";", ":d", "defer",
          "exit", "if", "else", "then", "begin", "while", "repeat", "constant",
          "variable"]
# fmt: on

print("\\ $ python3 programs/compiler/gen-tokens.py")
print()
print(": tok.EOF     EOF ;")
print(": tok.WORD      0 ;")
print(": tok.STRING    1 ;")
print()

current_num = 2
current_col = 0

for token in tokens:
    bol = " " if current_col > 0 else ""
    eol = "" if current_col < 3 else "\n"
    print("{}: tok.{: <8} {: >2} ;".format(bol, token, current_num), end=eol)

    current_num += 1
    current_col = current_col + 1 if current_col < 3 else 0

if current_col > 0:
    print()
print()


def do_print(s):
    global current_col

    if current_col + 1 + len(s) > 80:
        print("", end="\n")
        print(s, end="")
        current_col = len(s)
    else:
        print(" {:}".format(s), end="")
        current_col += 1 + len(s)


print("0", end="")
current_col = 1

for token in tokens:
    do_print("tok.{:}".format(token))
    do_print("0")
    for char in reversed(token):
        do_print(str(ord(char)))

print()
print()
print("tok.dict dict.fill")
