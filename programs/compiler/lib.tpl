\ lib.tpl
\
\ Simple words and data structures.



: -1 0 1 - ;
: -2 0 2 - ;
: -3 0 3 - ;
: -4 0 4 - ;

: p+ swap +p ;
: +over rot + swap ; \ a b c -- a+c b
: 2dup over over ;
: % 2dup / * - ;
: %/ 2dup % -rot / ; \ a b -- a%b a/b
: @1+! dup @ 1 + swap ! ; \ *to-increment --

\: <= > not ;
\: => < not ;
\: between? rot tuck < -rot < and ; \ n a b -- a<n<b?
: outside? rot tuck < -rot > or ;  \ n a b -- (n<a or n>b)
: ?dup dup if dup then ;
: round-pow2 1 - tuck + swap not and ; \ n 2^m -- l*2^m (l*2^m >= n)
rnew constant R \ permanent region used for persistent library structures

: unreachable "UNREACHABLE\n" 127 fail ;

: b%.. 2dup swap b% 8 >> ; \ buf n -- buf (n>>8)
: b%8  b%..                                    drop ; \ buf n -- buf
: b%16 b%.. b%..                               drop ; \ buf n -- buf
: b%32 b%.. b%.. b%.. b%..                     drop ; \ buf n -- buf
: b%64 b%.. b%.. b%.. b%.. b%.. b%.. b%.. b%.. drop ; \ buf n -- buf

: b!.. 2 pick 2 pick 2 pick b! 1 + rot 8 >> -rot ; \ n buf i -- (n>>8) buf (i+1)
: b!16 b!.. b!..                               drop drop drop ; \ n buf i --
: b!32 b!.. b!.. b!.. b!..                     drop drop drop ; \ n buf i --
: b!64 b!.. b!.. b!.. b!.. b!.. b!.. b!.. b!.. drop drop drop ; \ n buf i --



\ LINKED LISTS
\ These intrusive linked lists have an "offset" where the "next" field is.
\ The head of the list is not offset and must be initialized to 0.

: link over +p rot dup @ rot ! ! ; \ *head new-head offset --
: unlink                           \ *head offset -- unlinked   or   -- 0
  over @ dup if
    tuck +p @ rot !
  else nip nip
  then
;
: links.count \ *head offset -- count
  swap @ 0 -rot
  begin dup while over p+ @ rot 1 + -rot repeat
  drop drop
;



\ STACKS
\ Stacks live in a region.
\ They save popped pointers for reuse, but allocate when necessary.
\ Each stack element has length `length`. (Plus a "next" pointer at `-1 p+`.)

: stack..top         ; \ stack -- *top
: stack..region 1 p+ ; \ stack -- *region
: stack..length 2 p+ ; \ stack -- *length
: stack..unused 3 p+ ; \ stack -- *unused

: stack.new                 \ length region -- stack
  1 +over   4 over alloc
  0 over stack..top    !
    tuck stack..region !
    tuck stack..length !
  0 over stack..unused !
;

: stack.peek stack..top @ ; \ stack -- top   or   -- 0

: stack.pop                 \ stack --
  dup stack..top -1 unlink
  dup if
    swap stack..unused swap -1 link
  else drop drop then
;

: stack.push                \ stack -- new-top
  dup stack..unused -1 unlink
  dup if else
    drop dup
    dup  stack..length @
    swap stack..region @
    alloc 1 p+
  then
  tuck -1 link
;



\ CURSORS
\ Cursors are a higher-level interface on top of byte buffers.
\ They are intended for tokenization.
\ You "peek" a byte at a time, and consume it if it's part of the token.
\ Once you're done with this token, you "produce" the span a byte at a time.
\ You can also "skip" a span if you don't want to read it (e.g. whitespace).

: EOF -1 ;

: cursor..bytes         ; \ cursor -- *bytes
: cursor..position 1 p+ ; \ cursor -- *position
: cursor..span     2 p+ ; \ cursor -- *span

: cursor.zap \ cursor bytes --
    over cursor..bytes    !
  0 over cursor..position !
  0 swap cursor..span     !
;

: cursor.new \ bytes region -- cursor
  3 swap alloc
  tuck swap cursor.zap
;

: cursor.dup \ cursor region -- new-cursor
  3 swap alloc
  over cursor..bytes    @ over cursor..bytes    !
  over cursor..position @ over cursor..position !
  over cursor..span     @ over cursor..span     !
  nip
;

: cursor.peek \ cursor -- next-byte   or   -- EOF
  dup  cursor..span  @
  swap cursor..bytes @
  dup blen rot tuck
  > if b@ else drop drop EOF then
;

: cursor.consume cursor..span dup @ 1 + swap ! ; \ cursor --

: cursor.produce \ cursor -- next-byte   or   -- EOF
  dup  cursor..position @ tuck
  over cursor..span     @
  < if
    over 1 + over cursor..position !
    cursor..bytes @ swap b@
  else drop drop EOF then
;

: cursor.skip \ cursor --
  dup  cursor..span @
  swap cursor..position !
;

: cursor.len dup cursor..span @ swap cursor..position @ - ; \ cursor -- len
: cursor.save dup cursor..position @ swap ; \ cursor -- saved-pos cursor
: cursor.reset    cursor..position !      ; \ saved-pos cursor --

: cursor.produce-if-eq \ byte cursor -- bool
  cursor.save rot
  over cursor.produce =
  dup if nip nip
  else -rot cursor.reset then
;

: cursor.prefix? \ cursor bytes pos -- cursor bool
  begin 2 pick cursor.produce dup EOF <> while
    2 pick 2 pick b@ <> if drop drop 0 exit then
    1 +
  repeat
  drop drop drop -1
;

: cursor.copy \ bytes cursor --
  cursor.save swap -rot
  begin dup cursor.produce dup EOF <>
  while rot tuck b% swap repeat
  drop nip cursor.reset
;

: cursor.clear-bytes! \ cursor --
  dup    cursor..bytes @ bclear
  0 over cursor..position !
  0 swap cursor..span     !
;

: cursor.print \ cursor --
  cursor.save
  begin dup cursor.produce dup EOF <>
  while putc repeat drop
  cursor.reset
;
