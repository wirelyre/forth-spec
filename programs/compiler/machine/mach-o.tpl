: mach-o.header-size 512 ;

: mach-o.end
  object.section.main
  49  b%8 255 b%8       \ xor edi, edi
  184 b%8 33554433 b%32 \ mov eax, 0x02000001 (exit)
  15  b%8 5 b%8         \ syscall
  drop
;

: mach-o.file-size mach-o.header-size object.text-size + ;
: mach-o.text-len mach-o.file-size 4096 round-pow2 ;

: mach-o.layout
  object.section.main blen
  object.main->words !

  mach-o.text-len
  object.section.main blen -
  mach-o.header-size       -
  object.words->vars !
;

: mach-o.bytes

  object.file

  \ header
  4277009103 b%32          \ magic      : MH_MAGIC_64
  16777223   b%32          \ cputype    : CPU_TYPE_X86_64
  3          b%32          \ cpusubtype : CPU_SUBTYPE_I386_ALL
  2          b%32          \ filetype   : MH_EXECUTE
  4          b%32          \ ncmds      : 4
  480        b%32          \ sizeofcmds : 480
  0          b%32          \ flags      : 0
  0          b%32          \ reserved

  \ zero page
  25         b%32          \ cmd      : LC_SEGMENT_64
  72         b%32          \ cmdsize
  4997382908861767519 b%64 \ segname  : "__PAGEZERO"
  20306               b%64
  0          b%64          \ vmaddr   : 0x0000
  4096       b%64          \ vmsize   : 0x1000
  0          b%64          \ fileoff  : 0
  0          b%64          \ filesize : 0
  0          b%32          \ maxprot  : 0
  0          b%32          \ initprot : 0
  0          b%32          \ nsects   : 0
  0          b%32          \ flags    : 0

  \ text segment
  25         b%32          \ cmd      : LC_SEGMENT_64
  152        b%32          \ cmdsize  : 152
  92738097012575      b%64 \ segname  : "__TEXT"
  0                   b%64
  4096       b%64          \ vmaddr   : 0x1000
  mach-o.text-len     b%64 \ vmsize
  0          b%64          \ fileoff  : 0
  mach-o.file-size    b%64 \ filesize
  5          b%32          \ maxprot  : R-X
  5          b%32          \ initprot : R-X
  1          b%32          \ nsects   : 1
  0          b%32          \ flags    : 0

    \ text section
    128060447022943   b%64 \ sectname : "__text"
    0                 b%64
    92738097012575    b%64 \ segname  : "__TEXT"
    0                 b%64
    4608     b%64          \ addr     : 0x1200
    object.text-size  b%64 \ size
    512      b%32          \ offset   : 512
    9        b%32          \ align    : 512 (2^9)
    0        b%32          \ reloff   : 0
    0        b%32          \ nreloc   : 0
    0        b%32          \ flags    : 0
    0        b%32          \ reserved1
    0        b%32          \ reserved2
    0        b%32          \ (padding)

  \ data
  25         b%32          \ cmd      : LC_SEGMENT_64
  72         b%32          \ cmdsize  : 72
  71830128058207      b%64 \ segname  : "__DATA"
  0                   b%64
  mach-o.text-len 4096 +
                      b%64 \ vmaddr
  object.vars-len @ 4096 round-pow2
                      b%64 \ vmsize
  0          b%64          \ fileoff  : 0
  0          b%64          \ filesize : 0
  3          b%32          \ maxprot  : RW-
  3          b%32          \ initprot : RW-
  0          b%32          \ nsects   : 0
  0          b%32          \ flags    : 0

  \ thread entry
  5          b%32          \ cmd      : LC_UNIXTHREAD
  184        b%32          \ cmdsize  : 184
  4          b%32          \ flavor   : x86_THREAD_STATE64
  42         b%32          \ count    : x86_THREAD_STATE64_COUNT
                           \ (initial register values)
  0 b%64 0 b%64 0 b%64 0 b%64 \ r0-r15
  0 b%64 0 b%64 0 b%64 0 b%64
  0 b%64 0 b%64 0 b%64 0 b%64
  0 b%64 0 b%64 0 b%64 0 b%64
  4608 b%64                   \ rip
  0 b%64 0 b%64 0 b%64 0 b%64 \ rflags, cs, fs, gs

  drop

  object.file object.section.main  bapp
  object.file object.section.words bapp

  begin object.file blen 4096 <
  while 0 object.file b% repeat

;
