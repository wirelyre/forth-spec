bnew constant object.file

\ We will generate two text sections and two data sections.
\
\ TEXT:
\   main  (execute top-level words)
\   words (word definitions)
\ DATA:
\   vars  (constants and variables)
\   stack (the data stack)

bnew constant object.section.main
bnew constant object.section.words

\ Constants and variables will be initialized at runtime.
\ So the data sections will not take any space in the executable.
\ Furthermore, the stack size is independent of any generated code.
\ Thus we only need to keep track of the length of the variables section.

variable object.vars-len
       0 object.vars-len !

\ Steps for generating an object file:
\
\ 1. Layout
\    - determine length of object file header
\    - determine layout of segments in memory
\    - determine layout of sections
\
\ 2. Fix-ups
\    - fix up all references to words from main
\    - fix up all references to vars from main
\    - fix up some references to words from words
\    - fix up all references to vars from words
\
\ 3. Bytes
\    - write actual bytes of object file (headers, segments, footers, etc.)
\
\ Fix-ups are independent of the specific file format because we're generating
\ position-independent code.

\ numbers of bytes from the start of one section to the start of another
\ filled in during layout
variable object.main->words
variable object.words->vars

: object.text-size
  object.section.main  blen
  object.section.words blen +
;
