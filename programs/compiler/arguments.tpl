argc 1 + R alloc  constant args.files
                  variable args.output
0 args.output !

: args.usage           \ -- (NORETURN)
  "Usage:\n"
  "  tpl-compile <file> ... -o <output>\n"
  "  tpl-compile -h\n"
  0 fail
;

: args.required-output \ -- (NORETURN)
  "Error: required -o <output>\n"
  1 fail
;

: args.required-arg    \ option-char -- (NORETURN)
  "Error: parameter required for -" putc "\n"
  1 fail
;

: args.unknown-opt     \ option-char -- (NORETURN)
  "Error: unknown option -" putc "\n"
  1 fail
;



argv R cursor.new constant args.cur

: args.next \ -- bool
  args.cur cursor.peek EOF = if
    0
  else
    begin args.cur cursor.peek
    while args.cur cursor.consume repeat
    -1
  then
;

: args.done args.cur dup cursor.consume cursor.skip ; \ --



: args.parse
  args.files
  begin args.next while

    45 args.cur cursor.produce-if-eq if \ -
    \ option

      args.cur cursor.produce
      dup 104 = if args.usage then \ h
      dup 111 = if                 \ o
        args.done args.next
        if   args.cur R cursor.dup args.output !
        else 111 args.required-arg then
      else args.unknown-opt then
      drop

    else
    \ regular file
      args.cur R cursor.dup over !
      1 p+

    then

  args.done repeat
  0 swap !

  args.output @ if else args.required-output then
;

args.parse
