\ dictionary.tpl
\
\ A map from strings to single values.
\
\ Currently implemented as a hash list (a linked list using hashes).



\ HASHES
\ This hash function is called "djb2". It produces 32-bit hashes.
\ The upper 32 bits of a key are the length of the hashed string.
\ The full 64 bits are not known to have good statistical behavior.

: hash.djb2 \ cursor -- hash
  5381
  begin over cursor.produce dup EOF <>
  while -rot dup 5 << + rot xor repeat
  drop nip 4294967295 and
;

: hash.key \ cursor -- key
  dup cursor.len 32 << swap
  cursor.save
  dup hash.djb2
  -rot cursor.reset or
;



\ DICTIONARIES
\ These dictionaries are "hash lists".
\ Each entry in the list stores its hash in addition to the full key.
\ When searching, hash comparisons obviate almost all string comparisons.
\ The header field `full-keys` is a byte buffer containing each key in full.
\ Each entry's `full-key` is an offset into the `full-keys` buffer.

: dict..region              ;
: dict..full-keys      1 p+ ;
: dict..head           2 p+ ;

: dict..desired-key    3 p+ ;
: dict..desired-string 4 p+ ;

: dict.entry..next          ;
: dict.entry..key      1 p+ ;
: dict.entry..full-key 2 p+ ;
: dict.entry..value    3 p+ ;

: dict.new \ region -- dict
  5 over alloc
       tuck dict..region    !
  bnew over dict..full-keys !
  0    over dict..head      !
;

: dict.insert \ cur dict -- *value
  dup       dict..region    @ 4 swap alloc
  2dup swap dict..head      swap 0   link
  swap      dict..full-keys @
      dup blen     rot tuck dict.entry..full-key !
  rot dup hash.key rot tuck dict.entry..key      !
  -rot cursor.copy
  dict.entry..value
;

: dict.matches? \ dict entry -- bool
  dup dict.entry..key @
  rot tuck dict..desired-key @ <> if drop drop 0 exit then

  dup dict..desired-string @ cursor.save
  rot dict..full-keys @
  3 pick dict.entry..full-key @
  cursor.prefix? -rot cursor.reset nip
;

: dict.search \ cur dict -- *value   or   -- 0
  over hash.key over dict..desired-key    !
  tuck               dict..desired-string !
  dup dict..head @

  begin dup while
  2dup dict.matches? if dict.entry..value nip exit then
  dict.entry..next @ repeat

  nip
;



bnew R cursor.new constant dict.tmp

\ 0   value_n 0 key_n ... key_0   ...   value_0 0 key_n ... key_0    dict --
: dict.fill
  begin over while
    dict.tmp cursor.clear-bytes!
    begin over while
      swap dict.tmp cursor..bytes @ b%
      dict.tmp cursor.consume
    repeat nip
    dict.tmp over dict.insert rot swap !
  repeat drop drop
;
