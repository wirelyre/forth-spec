  R dict.new  constant parse.defs
1 R stack.new constant parse.flow



: parse.REGULAR  0 ;
: parse.DEFERRED 1 ;

defer parse.word
defer parse.string
defer parse.builtin
defer parse.stack-manip
defer parse.control
defer parse.end-of-file

: parse.file \ cur -- cur
  begin tok.next dup tok.EOF <> while
         dup tok.WORD   = if drop parse.word
    else dup tok.STRING = if drop parse.string
    else dup tok.drop   < if      parse.builtin
    else dup tok.:      < if      parse.stack-manip
    else                          parse.control
    then then then then
  repeat drop
  parse.end-of-file
;



: parse.number \ cursor -- cursor number true   or   -- cursor false
  cursor.save 0 swap

  begin dup cursor.produce dup EOF <> while
    dup 48 57 outside? if drop nip tuck cursor.reset 0 exit then
    48 - rot 10 * + swap
  repeat drop

  rot drop swap -1
;

:d parse.word \ cursor -- cursor
  dup parse.defs dict.search
  ?dup if              code.regular-word
  else parse.number if code.number
  else                 code.deferred-word
  then then
;

:d parse.string code.string ; \ cursor -- cursor




:d parse.builtin \ cursor token -- cursor
       dup tok.+    = if drop code.emit-+
  else dup tok.-    = if drop code.emit--
  else dup tok.*    = if drop code.emit-*
  else dup tok./    = if drop code.emit-/
  else dup tok.=    = if drop code.emit-=
  else dup tok.<>   = if drop code.emit-<>
  else dup tok.<    = if drop code.emit-<
  else dup tok.>    = if drop code.emit->
  else dup tok.or   = if drop code.emit-or
  else dup tok.xor  = if drop code.emit-xor
  else dup tok.and  = if drop code.emit-and
  else dup tok.not  = if drop code.emit-not
  else dup tok.<<   = if drop code.emit-<<
  else dup tok.>>   = if drop code.emit->>
  else dup tok.@    = if drop code.emit-@
  else dup tok.!    = if drop code.emit-!
  else dup tok.putc = if drop code.emit-putc
  else dup tok.fail = if drop code.emit-fail
  else unreachable
  then then then then then then then then then then then then then then then
  then then then
;

:d parse.stack-manip \ cursor token -- cursor
       dup tok.drop = if drop code.emit-drop
  else dup tok.nip  = if drop code.emit-nip
  else dup tok.dup  = if drop code.emit-dup
  else dup tok.over = if drop code.emit-over
  else dup tok.tuck = if drop code.emit-tuck
  else dup tok.swap = if drop code.emit-swap
  else dup tok.rot  = if drop code.emit-rot
  else dup tok.-rot = if drop code.emit--rot
  else dup tok.pick = if drop code.emit-pick
  else unreachable
  then then then then then then then then then
;

: parse.check-declare \ cursor -- cursor
  parse.flow stack.peek if
    "Error: '" cursor.print "' inside definition\n" 1 fail then
  tok.next dup tok.EOF = if
    "Error: unexpected end of file\n" 1 fail then
  dup tok.STRING = if
    "Error: unexpected string\n" 1 fail then
  tok.WORD <> if
    "Error: cannot redefine '" cursor.print "'\n" 1 fail then
;

: parse.check-define \ cursor -- cursor
  parse.check-declare
  dup parse.defs dict.search if
    "Error: cannot redefine '" cursor.print "'\n" 1 fail then
;

: parse.define \ cursor type -- cursor *created
  over parse.defs dict.insert
  2 R alloc tuck swap !
  tuck !
  1 p+
;

: parse.check-control \ cursor -- cursor
  parse.flow stack.peek if else
    "Error: '" cursor.print "' outside definition\n" 1 fail then
;

:d parse.control \ cursor token -- cursor

  dup tok.: = if drop
    parse.check-define
    parse.REGULAR parse.define
    parse.flow stack.push tok.: swap !
    code.newdef

  else dup tok.; = if drop
    parse.flow stack.peek dup if else
      "Error: ';' outside definition\n" 1 fail then
    @ tok.: <> if
      "Error: unterminated control flow in definition\n" 1 fail then
    parse.flow stack.pop
    code.enddef

  else dup tok.:d = if drop
    parse.check-declare
    dup parse.defs dict.search dup if else
      "Error: undeclared word '" cursor.print "'\n" 1 fail then
    @ dup @ parse.DEFERRED <> if
      "Error: cannot redefine '" drop cursor.print "'\n" 1 fail then
    parse.REGULAR over !
    1 p+
    parse.flow stack.push tok.: swap !
    code.defer-def

  else dup tok.defer = if drop
    parse.check-define
    parse.DEFERRED parse.define
    code.defer


  else dup tok.exit = if drop
    parse.check-control
    code.emit-exit


  else dup tok.if = if drop
    parse.check-control
    parse.flow stack.push tok.if swap !
    code.emit-if

  else dup tok.else = if drop
    parse.check-control
    parse.flow stack.peek @ tok.if <> if
      "Error: 'else' without matching 'if'\n" 1 fail then
    parse.flow stack.peek tok.else swap !
    code.emit-else

  else dup tok.then = if drop
    parse.check-control
    parse.flow stack.peek @ dup tok.if <> swap tok.else <> and if
      "Error: 'then' without matching 'if' or 'else'\n" 1 fail then
    parse.flow stack.pop
    code.emit-then


  else dup tok.begin = if drop
    parse.check-control
    parse.flow stack.push tok.begin swap !
    code.emit-begin

  else dup tok.while = if drop
    parse.check-control
    parse.flow stack.peek @ tok.begin <> if
      "Error: 'while' without matching 'begin'\n" 1 fail then
    parse.flow stack.peek tok.while swap !
    code.emit-while

  else dup tok.repeat = if drop
    parse.check-control
    parse.flow stack.peek @ tok.while <> if
      "Error: 'repeat' without matching 'while'\n" 1 fail then
    parse.flow stack.pop
    code.emit-repeat


  else dup tok.constant = if drop
    parse.check-define
    parse.REGULAR parse.define
    code.def-constant

  else dup tok.variable = if drop
    parse.check-define
    parse.REGULAR parse.define
    code.def-variable


  else unreachable
  then then then then then then then then then then then then then
;

:d parse.end-of-file
  parse.flow stack.peek if
    parse.flow stack.peek @ tok.: =
    if "Error: unterminated definition\n" 1 fail
    else "Error: unterminated control flow in definition\n" 1 fail
  then then
;
