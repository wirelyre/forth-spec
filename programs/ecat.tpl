\ like cat, but to stderr

variable arg.p \ byte position in argv
bnew constant filename

: 2dup over over ;
: inc-over swap 1 + swap ;

: read-filename
  filename bclear
  begin argv arg.p @ b@ while
    argv arg.p @ b@ filename b%
    arg.p @ 1 + arg.p !
  repeat
  arg.p @ 1 + arg.p !
;

: output-contents
  0
  begin 2dup swap blen < while
    2dup b@ putc
    1 +
  repeat
;

: main
  0 arg.p !
  begin arg.p @ argv blen < while
  read-filename
  filename fread output-contents
  repeat
;

main
