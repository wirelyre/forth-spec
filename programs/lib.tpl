: 2dup over over ;   \ a b -- a b a b
: % 2dup / * - ;     \ a b -- a%b
: %/ 2dup % -rot / ; \ a b -- a%b a/b

: -1 0 1 - ;
: MAX_INT -1 2 / ;

: is-neg? MAX_INT > ;
: remove-sign dup is-neg? tuck if 0 swap - then ; \ n -- 0 n   or   -n -- 1 n
: add-sign swap if 0 swap - then ;                \ 0 n -- n   or   1 n -- -n

\ signed division and modulus
: s/ remove-sign rot remove-sign rot / add-sign add-sign ;
: s% 2dup s/ * - ;

: .
  dup 0 = if   drop "0 " exit   then

  10 swap
  begin dup 0 > while 10 %/ repeat drop
  begin dup 10 = not while 48 + putc repeat
  drop " "
;
: s. remove-sign swap if "-" then . ;
