\ runtime.tpl
\
\ This file defines primitive words that have relatively complicated logic.
\
\ First `.` is defined in terms of arithmetic and `putc`.
\
\ Then memory primitives --- memory regions, byte buffers, and file operations
\ --- are defined in terms of system calls.
\
\ This file provides a runtime suitable for both for an interpreter and a
\ compiler. In particular, the memory primitives are defined efficiently.



: 2dup over over ;   \ a b -- a b a b
: % 2dup / * - ;     \ a b -- a%b
: %/ 2dup % -rot / ; \ a b -- a%b a/b

\ This is not particularly optimized. `.` should only be called rarely --- for
\ instance, as part of an error message immediately before `fail`.
: .
  dup 0 = if   drop "0 " exit   then

  10 swap
  begin dup 0 > while 10 %/ repeat drop
  begin dup 10 = not while 48 + putc repeat
  drop " "
;



\ Dynamic memory is handled by several abstraction layers.
\
\ At the bottom we have *blocks*. The length of a block in bytes is one of
\ 4096, 8192, 16384, etc. The OS provides blocks of memory when we ask for
\ them. When a block of memory is no longer in use, we keep it for future
\ reuse, before asking the OS for another block.
\
\ Next we have the *pointer list*. This is a collection of pointers to
\ high-level memory regions and byte buffers. It is built from 4096-byte
\ blocks. There is a linked list of pointers that haven't been allocated yet.
\
\ Finally, we have *memory regions* and *byte buffers*. These are composed of
\   1. A pointer in the pointer list, which points to
\   2. A block (or blocks) of memory.
\
\ These are all described in more detail near their implementations below.
\
\
\ Example:
\
\     Unused blocks:
\       +--------------+   +--------------+
\       | (4096 bytes) |-->| (8192 bytes) |--> NULL
\       +--------------+   +--------------+
\
\     Pointer list:
\       +------+  +------+  +------+  +--------+
\       |region|  |region|  |buffer|  | unused |--> ... --> NULL
\       +------+  +------+  +------+  +--------+
\          |         |         |    +---------------+
\          |         +-> NULL  +--> | (16384 bytes) |
\          |                        +---------------+
\          |
\          |    +--------------+   +--------------+
\          +--> | (4096 bytes) |-->| (8192 bytes) |--> NULL
\               +--------------+   +--------------+



\ BLOCKS
\
\ The block allocator is based around a linked list of unused blocks.
\
\   unused_blocks: [size] [next]
\                           |
\     v---------------------+
\   block_0:     [size] [next]
\            ...
\   block_n:     [size] [NULL]
\
\ When a block is allocated, we search through the linked list to find a block
\ of exactly the right length. If found, we remove it from the list and return
\ it. Otherwise, we ask the OS for a block of that length.
\
\ Before searching the list, the requested length is rounded up to the nearest
\ power of 2, and at minimum 4096 (a common size for hardware memory pages).
\
\ The linked list is "intrusive", so each entry in the list structure is also
\ the first 16 bytes of an allocated block. The head of the linked list,
\ unused_blocks, is only large enough to hold a list entry header.
\
\ When freeing a block, the first 8 bytes of the block must be its total
\ length. Conversely, if a block is not intended to be freed, the first 8 bytes
\ may be used.

variable UNUSED-BLOCKS
0 UNUSED-BLOCKS !

: ALLOC-BLOCK \ len -- block
  ROUND_UP

  \ loop through unused blocks
  UNUSED-BLOCKS begin dup @ while
    dup @ @ rot tuck = if
      drop dup @
      dup 8 + @ rot !
      exit
    then
  swap @ 8 + repeat drop

  \ no unused blocks of the correct length
  dup OS_ALLOC
  tuck !
;

: FREE-BLOCK UNUSED-BLOCKS @ over 8 + ! UNUSED-BLOCKS ! ; \ block --



\ POINTERS
\
\ The pointer list is based around a linked list of unused pointers.
\
\   unused_pointers: [ptr_0] --> ... --> [ptr_n] --> NULL
\
\ If the head of the list is NULL, we need to allocate a block and add its
\ memory to the list.

variable UNUSED-POINTERS
0 UNUSED-POINTERS !

: ALLOC-POINTER \ -- ptr
  UNUSED-POINTERS @

  dup 0 = if
    drop
    4095 ALLOC-BLOCK

    0 begin dup 511 < while
      2dup swap +p dup 8 + swap !
    1 + repeat drop

    511 over +p 0 swap !
  then

  dup @ UNUSED-POINTERS !
;

: FREE-POINTER UNUSED-POINTERS @ over ! UNUSED-POINTERS ! ; \ ptr --



\ MEMORY REGIONS
\
\ A memory region is a primitive data structure. You can:
\   `rnew`   to create a region
\            - `dup`ing a region yields a new reference to the same region
\   `alloc`  a number of cells (8 bytes each) in a region
\            - cells from a single `alloc` call are contiguous in memory,
\              but cells from multiple calls need not be
\   `rclear` to clear a region and invalidate the cells `alloc`ed in it
\            - this returns the blocks used by the region to the free list
\
\ A region by itself is a pointer created from the pointer list. It points to a
\ linked list of blocks:
\
\    region: [block_0]
\                |
\                v
\       block_0: [size] [next] [used] [... data ...]
\          ...
\       block_n: [size] [NULL] [used] [... data ...]
\
\ The first block, block_0, is "active". New allocations in the region go in
\ the active block, if there is space. Otherwise, a new block is allocated and
\ prepended to the list.
\
\ It is also possible that block_0 is NULL --- that is, that the value of the
\ region pointer is zero. In that case, no memory has been allocated to the
\ region at all (besides the pointer itself). This is true for an `rnew` region
\ or an `rclear`ed region.
\
\ "used" contains the number of bytes, starting at the beginning of the block,
\ that are in use. For an empty block, "used" is 24.

: rnew ALLOC-POINTER 0 over ! ; \ -- region

: rclear \ region --
  dup @
  0 rot !

  \ free each block
  begin dup while
    dup 8 + @ swap
    FREE-BLOCK
  repeat drop
;

: LINK-NEW-BLOCK \ length region --
  over 24 + ALLOC-BLOCK \ block
  over @ over 8 + !
  24 over 16 + !
  swap !
  drop
;

: alloc \ n r -- ptr
  swap 0 +p swap \ bytes r

  \ link new block if region is NULL or not large enough
  dup @ 0 =
  if 2dup LINK-NEW-BLOCK
  else
    dup @ dup @ swap 8 + @ \ capacity used
    3 pick +               \ capacity proposed-used
    < if 2dup LINK-NEW-BLOCK then
  then

  @ dup 16 + @ \ bytes block current-pos
  rot over +   \ block current-pos new-pos
  -rot over +  \ new-pos block fragment
  -rot 16 + !
;



\ BYTE BUFFERS
\
\ A byte buffer is a primitive data structure. You can:
\   `bnew`   to create an empty buffer
\            - `dup`ing a buffer yields a new reference to the same buffer
\   `blen`   to read the number of bytes currently in the buffer
\   `bclear` to delete all bytes in the buffer
\   `b@`     to read a byte from the buffer
\            - the byte to read must be within the buffer's current length
\   `b!`     to set a byte in the buffer
\            - the byte to set must be within the buffer's current length
\   `b%`     to push a single byte to the end of the buffer
\   `bapp`   to append the contents of one buffer to the end of another
\
\ A buffer is a pointer created from the pointer list. It points into the
\ *middle* of a single block:
\
\    buffer: [block] --------+
\                            |
\                            v
\       block: [size] [used] [... data ...]
\
\ "used" contains the number of bytes of "data" that are in use. For an empty
\ block, "used" is 0.
\
\ "block" is never NULL --- the buffer pointer always points into a single
\ block.
\
\ Byte buffers only have a single block so that it is fast to randomly access
\ any byte in the buffer. Every time a new block is allocated, the contents of
\ the buffer have to be copied to the new block. `bclear` reduces the number of
\ copies by *not* deallocating the block.

: bnew \ -- buf
  ALLOC-POINTER
  16 ALLOC-BLOCK
  0 over 8 + !
  16 + swap tuck !
;

: blen @ 8 - @ ;          \   b   -- len
: bclear @ 8 - 0 swap ! ; \   b   --
: b@ swap @ + C@ ;        \   b i -- c
: b! swap @ + C! ;        \ c b i --

: RESIZE-BYTE-BUFFER \ old new-cap --
  ALLOC-BLOCK 16 + \ *old-buf new-buf

  over @ 8 - \ copy-from
  over   8 - \ copy-to
  over 8 - @ \ copy-length
  COPY

  \ swap out block in pointer
  swap dup @ -rot !

  16 - FREE-BLOCK
;

: b% \ byte buf --
  dup blen 16 + \ block-bytes-used
  over @ 16 - @ \ block-capacity

  \ resize if block is full
  = if dup blen 17 + over swap RESIZE-BYTE-BUFFER then

  \ set byte
  tuck dup blen b!   \ buf
  \ increment length
  dup blen 1 +       \ buf new-len
  swap @ 8 - !
;

: bapp \ dest src --

  over @ dup 16 - @ \ dest-cap
  swap 8 - @ 16 +   \ dest-used
  2 pick blen       \ src-new

  \ resize if the proposed size wouldn't fit
  + > if
    over 
    dup @ 8 - @ 16 +
    2 pick blen +
    RESIZE-BYTE-BUFFER
  then

  2dup

  \ do copy
  @ dup 8 - @ rot \ copy-from copy-length dest
  @ dup 8 - @ +   \ copy-from copy-length copy-to
  swap COPY

  \ update length
  blen swap @ 8 - \ src-len *dest-len
  dup @ rot + swap !
;



\ FILE I/O

: fread \ path -- data   or   path -- 0
  \ add terminating 0 to path
  0 over b%

  @ dup OS_OPEN \ path FILE

  \ remove terminating 0 from path
  swap 8 -
  dup @ 1 -
  swap !

  dup 0 = if exit then

  dup OS_LEN       \ length
  dup 16 + 
  ALLOC-BLOCK 16 + \ buffer
  2dup 8 - !

  dup rot 3 pick \ FILE buffer buffer length FILE
  OS_READ nip    \ buffer

  ALLOC-POINTER tuck !
;

: fwrite \ data path -- success?
  \ add terminating 0 to path
  0 over b%
  tuck

  swap @    \ data
  dup 8 - @ \ data-len
  rot @     \ path

  OS_WRITE \ path data data-len path -- path success?

  \ remove terminating 0 from path
  swap
  dup 8 - @ 1 -
  swap 8 - !
;

\ COMMAND-LINE ARGUMENTS
\ at program entry the stack is:
\     argv argc

constant ARGC
constant ARGV

: print-top-2 2dup . . "\n" ;

: argc ARGC if ARGC 1 - else 0 then ;

: argv
  bnew
  ARGV @ 0 = if exit then \ empty buffer if argc == 0

  \ loop through args, starting at &argv[1]
  1 ARGV +p

  \ buf argv
  begin dup @ while
    2dup @

    \ buf argv buf arg
    begin
      dup C@
      rot dup -rot b% swap
    dup C@ while 1 + repeat \ next byte if not null
    drop drop

    1 swap +p               \ next arg
  repeat

  drop
;
