.intel_syntax noprefix

.globl getc, putc
.globl putc_, fail_, os_alloc_, os_open_, os_length_, os_read_, os_write_



.macro Spop reg
    mov \reg, [r12-8]
    sub r12, 8
.endm
.macro Spush reg
    mov [r12], \reg
    add r12, 8
.endm



getc:
    push rbp
    call os_getc
    pop rbp
    ret

putc:
    push rdi
    call os_putc
    pop rdi
    ret

putc_:
    push rdi
    Spop rdi
    call os_putc
    pop rdi
    ret

fail_:
    Spop rdi
    jmp os_exit
    # does not return

os_alloc_:
    push rdi
    Spop rdi
    call os_alloc
    Spush rax
    pop rdi
    ret

os_open_:
    push rdi
    Spop rdi # len
    call os_open
    Spush rax
    pop rdi
    ret

os_length_:
    push rdi
    Spop rdi # FILE
    call os_length
    Spush rax
    pop rdi
    ret

os_read_:
    push rdi
    Spop rdi # FILE
    Spop rsi # len
    Spop rdx # into
    call os_read
    pop rdi
    ret

os_write_:
    push rdi

    Spop rdi # path
    Spop rsi # len
    Spop rdx # from
    call os_write
    Spush rax # success?

    pop rdi
    ret
