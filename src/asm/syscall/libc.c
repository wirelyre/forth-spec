#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>

uint64_t os_getc() {
    char c = getchar();
    if (c == EOF)
        return -1;
    else
        return (uint8_t) c;
}

void os_putc(char c) {
    fputc(c, stderr);
}

void *os_alloc(size_t s) {
    return malloc(s);
}

noreturn void os_exit(int code) {
    exit(code);
}

FILE *os_open(const char *path) {
    return fopen(path, "rb");
}
uint64_t os_length(FILE *f) {
    fseek(f, 0, SEEK_END);
    return ftell(f);
}
void os_read(FILE *f, uint64_t len, void *into) {
    fseek(f, 0, SEEK_SET);
    fread(into, 1, len, f);
    fclose(f);
}

uint64_t os_write(const char *path, uint64_t len, const void *from) {
    FILE *f = fopen(path, "r");

    if (f) { // file exists
        fclose(f);
        return 0;
    }

    f = fopen(path, "wb");
    fwrite(from, 1, len, f);
    fclose(f);

    return -1;
}
