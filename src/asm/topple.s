.intel_syntax noprefix

.globl main, _main



# INTERPRETER

.comm dictionary, 64 * 1024 * 1024
.comm stack, 8 * 4096

main:
_main:
    push rbp

    # Within the interpreter and primitive words we maintain:
    #   - r12, the data stack (the current top)
    #   - r13, the code area  (the current end)
    #   - r14, the last word in the dictionary
    #   - r15, whether we're interpreting (0) or compiling (1)
    #
    # Both the data stack and code area grow upwards.
    #
    # r12-r15 are callee-saved, so we can call C and syscalls freely.
    #
    # The bytes at [r13] (above the current dictionary) are also used as a
    # buffer for the last word read. This avoids some tricky data movement when
    # defining a new word.
    #
    # The format of a dictionary entry, in bytes, is:
    #   0                    23           24         32
    #   [0-terminated name]  [immediate?] [routine]  [next]
    #
    # The dictionary is a linked list of words. Each word is either "immediate"
    # or not. Immediate words are executed right away, whether we're currently
    # interpreting or compiling. Non-immediate words are either executed or
    # compiled into the current word, as appropriate.
    #
    # For example, when we read `;`, we have work to do *right now*. We have to
    # insert the `exit` word, and change r15 back to 0. Immediate words are
    # basically the control flow primitives; all other words, including
    # colon-defined words, are non-immediate.
    #
    # When executing a user-defined word, we need to keep track of where we are
    # in the code area.
    #                                       next
    #                                       v-----
    #   [word header]     [literal] [65]    [putc]    [exit]
    # We maintain rdi as the pointer to the next word to execute. Primitives
    # that use rdi are responsible for saving it. Primitives that take a word
    # as an argument (e.g. literal numbers and `if`-like control flow) are
    # responsible for updating rdi as appropriate.

    push r12
    push r13
    push r14
    push r15
    mov r12, [rip + stack@GOTPCREL]
    mov r13, [rip + dictionary@GOTPCREL]
    xor r14, r14
    xor r15, r15

    mov [r12],   rsi # initialize stack
    mov [r12+8], rdi # -- argv argc
    add r12, 16

    call set_up_primitives

    L_.main.loop:

    call read_word
    test rax, rax
    jz L_.main.end   # exit if EOI

    mov al, byte ptr [r13]
    cmp al, '"'
    jne L_.main.find_word
    call handle_string
    jmp L_.main.loop

    L_.main.find_word:
    call find_word
    jz L_.main.num       # found word?
        call handle_word # yes, rdx is the definition
        jmp L_.main.loop
    L_.main.num:
        xor eax, eax
        mov rdi, r13     # no, rax is 0 and [rdi] is a number string
        call handle_num
        jmp L_.main.loop

    L_.main.end:
    mov rax, 0
    pop r15
    pop r14
    pop r13
    pop r12
    pop rbp
    ret

handle_string:
    # For now, we assume that all strings are compiled into a word.

    lea rsi, [r13+8]
    mov rdi, [r13+1] # unaligned loads here...

    L_.handle_string.loop:
    mov rdx, [rsi+1] # ... and here, which skip the leading "
    mov [rsi], rdi
    mov rdi, rdx
    mov ecx, 8

    L_.handle_string.check_null:
    mov al, [rsi]
    inc rsi
    test al, al
    jz L_.handle_string.end
    dec ecx
    jz L_.handle_string.loop
    jmp L_.handle_string.check_null

    L_.handle_string.end:
    add rsi, 7
    and rsi, ~7
    lea rdi, [rbp + 184] # do_string
    mov [r13], rdi
    mov r13, rsi
    ret

handle_word:
    lea rsi, [rdx+24]      # [rsi] is the function pointer

    mov al, [rdx+23]
    test al, al            # immediate?
    jnz L_.handle_word.run # yes, call word

    test r15, r15          # interpreting?
    jz L_.handle_word.run  # yes, call word

    mov [r13], rsi         # compile the word into the current definition
    add r13, 8
    ret

    L_.handle_word.run:
    push rbp               # call the word
    call [rsi]
    pop rbp
    ret

handle_num:
    movzx edx, byte ptr [rdi]   # parse ASCII number [rdi] into rax
    test edx, edx               # continue at terminating 0
    jz L_.handle_num.end

    # ensure that this is an ASCII digit
    cmp edx, '0'
    jb L_.handle_num.fail
    cmp edx, '9'
    ja L_.handle_num.fail

    imul rax, 10
    lea rax, [rax + rdx - '0']
    inc rdi
    jmp handle_num

    L_.handle_num.end:
    test r15, r15
    jnz L_.handle_num.compile   # interpreting?
        mov [r12], rax          # yes, push to data stack
        add r12, 8
        ret
    L_.handle_num.compile:      # no, write to dictionary
        lea rcx, [rbp + 64] # do_num
        mov [r13], rcx
        mov [r13+8], rax
        add r13, 16
        ret

    L_.handle_num.fail:
        xor edx, edx # set Z flag
        ret



read_word:
    push r14
    mov r14, r13

    L_.read_word.skip:
    call getc
    cmp al, '\\'
    je L_.read_word.comment
    cmp al, ' '
    je L_.read_word.skip
    cmp al, 0x0d            # carriage return
    je L_.read_word.skip
    cmp al, '\n'
    je L_.read_word.skip
    cmp al, '"'
    je L_.read_word.string
    test rax, rax
    jns L_.read_word.main
    xor eax, eax
    pop r14
    ret

    L_.read_word.comment:
    call getc
    cmp al, 0x0d            # carriage return
    je L_.read_word.skip
    cmp al, '\n'
    je L_.read_word.skip
    jmp L_.read_word.comment

    L_.read_word.string:
    mov [r14], al
    inc r14
    call getc
    cmp al, '\\'
    je L_.read_word.escape
    cmp al, '"'
    jne L_.read_word.string
    jmp L_.read_word.end

    L_.read_word.escape:
    call getc
    mov cl, '\n'
    cmp al, 'n'
    cmove ax, cx
    jmp L_.read_word.string

    L_.read_word.main:
    mov [r14], al
    inc r14
    call getc
    cmp al, '!'
    jge L_.read_word.main

    L_.read_word.end:
    mov byte ptr [r14], 0
    pop r14
    ret

strcmp:
    mov al, [rdi]
    mov cl, [rsi]
    inc rdi
    inc rsi
    cmp al, cl
    jne L_.strcmp.ne # str1 != str2
    test al, al
    jnz strcmp       # *str1 != 0
    mov eax, 1       # str1 == str
    ret
    L_.strcmp.ne:
    mov eax, 0
    ret

find_word:
    mov rdx, r14

    L_.find_word.loop:
    test rdx, rdx
    jz L_.find_word.end

    mov rsi, rdx
    mov rdi, r13
    call strcmp
    test rax, rax
    jnz L_.find_word.end
    mov rdx, [rdx+32]
    jmp L_.find_word.loop

    L_.find_word.end:
    ret

print_word:
    push r14
    mov r14, r13

    L_.loop:
    movzx edi, byte ptr [r14]
    test edi, edi
    jz L_.end

    call putc

    inc r14
    jmp L_.loop

    L_.end:
    mov rdi, 10
    call putc

    pop r14
    ret



do_colon:
    push rdi
    lea rdi, [rsi + 16]

    L_.do_colon.loop:
    mov rsi, [rdi]
    add rdi, 8
    call [rsi]
    jmp L_.do_colon.loop

exit_:
    add rsp, 8
    pop rdi
    ret

do_num:
    mov rax, [rdi]
    mov [r12], rax
    add rdi, 8
    add r12, 8
    ret

do_jump:
    mov rdi, [rdi]
    ret

do_branch:
    mov rax, [r12-8]   # pop rax
    sub r12, 8
    test rax, rax      # rdi = if rax != 0
    lea rdi, [rdi+8]   #   then rdi + 8
    cmovz rdi, [rdi-8] #   else *rdi
    ret

do_string:
    movzx eax, byte ptr [rdi]
    inc rdi
    test eax, eax
    jz L_.do_string.end
    push rdi
    mov edi, eax
    call putc
    pop rdi
    jmp do_string

    L_.do_string.end:
    add rdi, 7
    and rdi, ~7
    ret



set_up_primitives:

    .macro Wlink name addr immediate
    mov rax, \name
    lea rcx, [rip + \addr]
    mov qword ptr [r13], rax
    mov byte ptr  [r13+23], \immediate
    mov qword ptr [r13+24], rcx
    mov qword ptr [r13+32], r14
    mov r14, r13
    add r13, 40
    .endm

    .macro Wregular name addr
    Wlink \name, \addr, 0
    .endm
    .macro Wimmediate name addr
    Wlink \name, \addr, 1
    .endm

    # basic words - cannot be used directly in colon definitions (except exit)
    # These are saved at an offset from rbp so that control flow words can
    # refer to them without looking through the dictionary each time.
    mov rbp, r13
    Wregular 0x74697865,     exit_     # exit
    Wregular 0x4d554e,       do_num    # NUM
    Wregular 0x504d554a,     do_jump   # JUMP
    Wregular 0x48434e415242, do_branch # BRANCH
    Wregular 0x474e49525453, do_string # STRING

    Wregular   0x2b,           add_    # +
    Wregular   0x2d,           sub_    # -
    Wregular   0x2a,           mul_    # *
    Wregular   0x2f,           div_    # /
    Wregular   0x3c,           lt_     # <
    Wregular   0x3c3c,         shl_    # <<
    Wregular   0x3e3e,         shr_    # >>
    Wregular   0x702b,         addp_   # +p
    Wregular   0x40,           get_    # @
    Wregular   0x21,           set_    # !
    Wregular   0x63747570,     putc_   # putc
    Wregular   0x6c696166,     fail_   # fail
    Wregular   0x3a,           colon_     # :
    Wimmediate 0x3b,           semicolon_ # ;
    Wregular   0x643a,         colon_d_   # :d

    Wregular   0x4043,             cget_      # C@
    Wregular   0x2143,             cset_      # C!
    Wregular   0x50555f444e554f52, round_up_  # ROUND_UP
    Wregular   0x59504f43,         copy_      # COPY
    Wregular   0x434f4c4c415f534f, os_alloc_  # OS_ALLOC
    Wregular   0x4e45504f5f534f,   os_open_   # OS_OPEN
    Wregular   0x4e454c5f534f,     os_length_ # OS_LEN
    Wregular   0x444145525f534f,   os_read_   # OS_READ
    Wregular   0x45544952575f534f, os_write_  # OS_WRITE
    Wregular   0x524f4e,           nor_       # NOR
    Wregular   0x4b43415453,       stack_     # STACK
    Wregular   0x54434944,         dict_      # DICT
    Wregular   0x454c49504d4f43,   compile_   # COMPILE
    Wregular   0x4d4d49,           immediate_ # IMM
    Wimmediate 0x27,               quote_     # '

    ret



.macro Spop reg
    mov \reg, [r12-8]
    sub r12, 8
.endm
.macro Spush reg
    mov [r12], \reg
    add r12, 8
.endm



# REGULAR PRIMITIVES

add_:
    Spop rax
    add [r12-8], rax
    ret

sub_:
    Spop rax
    sub [r12-8], rax
    ret

mul_:
    Spop rax
    mov rcx, [r12-8]
    imul rcx, rax
    mov [r12-8], rcx
    ret

div_:
    xor rdx, rdx
    Spop rcx
    mov rax, [r12-8]
    div rcx
    mov [r12-8], rax
    ret

lt_:
    Spop rax
    cmp [r12-8], rax
    mov rax, 0
    setb al
    neg rax
    mov [r12-8], rax
    ret

shl_:
    Spop rcx
    shl qword ptr [r12-8], cl
    ret

shr_:
    Spop rcx
    shr qword ptr [r12-8], cl
    ret

nor_:
    Spop rax
    or rax, [r12-8]
    not rax
    mov [r12-8], rax
    ret

stack_:
    Spush r12
    ret

dict_:
    Spush r13
    ret

compile_:
    Spop rax
    mov [r13], rax
    add r13, 8
    ret

immediate_:
    mov byte ptr [r14+23], 1
    ret

addp_:
    Spop rax
    mov rcx, [r12-8]
    lea rax, [rax + 8*rcx]
    mov [r12-8], rax
    ret

get_:
    mov rax, [r12-8]
    mov rax, [rax]
    mov [r12-8], rax
    ret

set_:
    mov rax, [r12-8]
    mov rcx, [r12-16]
    mov [rax], rcx
    sub r12, 16
    ret

cget_: # reads a single "character" (byte)
    mov rax, [r12-8]
    movzx rax, byte ptr [rax]
    mov [r12-8], rax
    ret

cset_: # writes a single "character" (byte)
    Spop rax # address
    Spop rcx # byte
    mov [rax], cl
    ret



# FANCY PRIMITIVES

copy_:
    push rdi
    Spop rcx # len
    Spop rdi # to
    Spop rsi # from
    rep movsb
    pop rdi
    ret

quote_:
    push rdi
    call read_word
    call find_word
    add rdx, 24

    test r15, r15
    jz L_.quote_.interpret

    # compiling, move to dictionary
    lea rcx, [rbp + 64] # do_num
    mov [r13], rcx
    mov [r13+8], rdx
    add r13, 16
    pop rdi
    ret

    L_.quote_.interpret:
    Spush rdx # interpreting, push to stack now
    pop rdi
    ret

colon_:
    push rdi

    call read_word
    call find_word

    lea rax, [rip + do_colon]
    mov byte ptr [r13+23], 0
    mov qword ptr [r13+24], rax
    mov qword ptr [r13+32], r14
    mov r14, r13
    add r13, 40

    mov r15, 1

    pop rdi
    ret

semicolon_:
    lea rax, [rbp + 24] # exit
    mov qword ptr [r13], rax
    add r13, 8
    xor r15, r15
    ret

colon_d_:
    push rdi

    call read_word
    call find_word # rdx - definition

    lea rax, [rip + do_colon]
    mov [rdx+24], rax
    lea rax, [rbp + 104] # do_jump
    mov [rdx+40], rax
    mov [rdx+48], r13

    mov r15, 1

    pop rdi
    ret

round_up_: # n -- MIN(round_up_power_of_2(n), 4096)
    Spop rax

    bsr rcx, rax  # log_2
    inc cl        # round up
    mov eax, 1    # 2^cx
    shl rax, cl
    mov ecx, 4096 # at least 4096
    cmp rax, rcx
    cmovb rax, rcx

    Spush rax
    ret
