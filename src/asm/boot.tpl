\ boot.tpl
\
\ This file defines primitive words for simple operations. It is only useful
\ for bootstrapping most of the language from a small interpreter.

: not 0 NOR ;
: NEG not 1 + ;

: drop STACK ! ;
: pick 2 + NEG STACK +p @ ;

: dup  0 pick ;
: over 1 pick ;
: swap over over STACK 32 - ! STACK 16 - ! ;
: tuck swap over ;
: nip  swap drop ;
: rot  2 pick 2 pick 2 pick STACK 40 - ! STACK 40 - ! STACK 16 - ! ;
: -rot rot rot ;

: or NOR not ;
: and not swap not NOR ;
: xor over over and -rot NOR NOR ;

: > swap < ;
: <> over over < -rot > or ;
: = <> not ;

: if     ' BRANCH COMPILE DICT 0    COMPILE             ; IMM
: else   ' JUMP   COMPILE DICT 0    COMPILE DICT rot  ! ; IMM
: then                                      DICT swap ! ; IMM
: begin                   DICT                          ; IMM
: while  ' BRANCH COMPILE DICT 0    COMPILE             ; IMM
: repeat ' JUMP   COMPILE      swap COMPILE DICT swap ! ; IMM

: DELAY-; exit ; ' ; DICT 16 - !
: defer    : ' JUMP COMPILE 0 COMPILE DELAY-; ;
: constant : ' NUM  COMPILE   COMPILE DELAY-; ;
: variable DICT 0 COMPILE constant ;
