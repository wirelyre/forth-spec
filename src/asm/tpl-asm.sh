#!/usr/bin/env sh

INTERPRETER=$(dirname "$0")/asm/tpl-asm
LIBRARY=$(dirname "$0")/asm/lib.tpl

cat $LIBRARY - | "$INTERPRETER" $@
