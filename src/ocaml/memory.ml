open Base
open Stdint

exception IllegalPath
exception IllegalPointerOffset
exception StalePointer
exception OutOfBoundsByteBufferAccess
exception UndefinedAccess
exception ZeroLengthAlloc

type value =
  | Int of Uint64.t
  | Bytes of bytes
  | Region of region
  | Pointer of pointer

and bytes = {mutable data: Bytes.t; mutable len: int}

and region = fragment list ref

and pointer = fragment * int

and fragment = value Option.t Array.t Option.t ref

module Bytes = struct
  let create () = {data= Bytes.create 16; len= 0}
  let clear bytes = bytes.len <- 0
  let len bytes = bytes.len

  let ensure_len bytes required expand_to =
    if required > Bytes.length bytes.data then (
      let new_data = Bytes.create expand_to in
      Bytes.blito ~src:bytes.data ~dst:new_data () ;
      bytes.data <- new_data )

  let get bytes idx =
    if idx >= bytes.len then raise OutOfBoundsByteBufferAccess
    else Bytes.get bytes.data idx

  let set bytes idx c =
    if idx >= bytes.len then raise OutOfBoundsByteBufferAccess
    else Bytes.set bytes.data idx c

  let push bytes c =
    ensure_len bytes (bytes.len + 1) (bytes.len * 2) ;
    Bytes.set bytes.data bytes.len c ;
    bytes.len <- bytes.len + 1

  let append this other =
    let total_len = this.len + other.len in
    ensure_len this total_len (total_len * 2) ;
    Bytes.blito ~src:other.data ~dst:this.data ~dst_pos:this.len () ;
    this.len <- total_len
end

module Region = struct
  let create () : region = ref []

  let alloc region len =
    if len = 0 then raise ZeroLengthAlloc
    else
      let new_frag = ref (Some (Array.create ~len None)) in
      region := new_frag :: !region ;
      (new_frag, 0)

  let clear region =
    List.iter !region ~f:(fun frag -> frag := None) ;
    region := []
end

module Pointer = struct
  let access f = match !f with None -> raise StalePointer | Some arr -> arr

  let get (frag, offset) =
    let arr = access frag in
    match arr.(offset) with None -> raise UndefinedAccess | Some v -> v

  let set (frag, offset) v =
    let arr = access frag in
    arr.(offset) <- Some v

  let add (frag, offset) addend =
    let arr = access frag in
    let new_offset = offset + addend in
    if new_offset < 0 || Array.length arr <= new_offset then
      raise IllegalPointerOffset
    else (frag, new_offset)
end

module File = struct
  let initialized_of_bytes bytes = Base.Bytes.subo ~len:bytes.len bytes.data

  (* TODO: Is unprintable ASCII allowed? *)
  let path_of_bytes bytes =
    let path = Base.Bytes.To_string.subo ~len:bytes.len bytes.data in
    if String.exists path ~f:(fun c -> Char.(c > '\x7f')) then raise IllegalPath ;
    path

  let read path =
    let path = path_of_bytes path in
    try
      let str = Stdio.In_channel.(with_file ~binary:true path ~f:input_all) in
      let data = Base.Bytes.From_string.subo str in
      Bytes {data; len= Base.Bytes.length data}
    with Sys_error _ -> Int Uint64.zero

  let write path data =
    let path = path_of_bytes path in
    let data = initialized_of_bytes data in
    try
      Stdio.Out_channel.with_file ~binary:true ~append:false
        ~fail_if_exists:true path ~f:(fun chan ->
          Stdio.Out_channel.output_bytes chan data) ;
      true
    with Sys_error _ -> false
end
