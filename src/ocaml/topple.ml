open Base
open Stdio
open Stdint

let format_val = function
  | Memory.Int i -> Stdint.Uint64.to_string i
  | Bytes _ -> "(bytes)"
  | Region _ -> "(region)"
  | Pointer _ -> "(pointer)"

let print_cause =
  let format =
    Out_channel.eprintf
      "\x1b[1m\x1b[31mError:\x1b[30m\x1b(B\x1b[m %s\n\
       \x1b[1m\x1b[33mHelp:\x1b[30m\x1b(B\x1b[m %s\n\n" in
  let unexpected v = format ("Unexpected value " ^ format_val v) in
  function
  | Memory.IllegalPath ->
      format "Illegal path" "Paths must consist entirely of ASCII characters."
  | Memory.IllegalPointerOffset ->
      format "Illegal pointer offset"
        "Pointers cannot be offset outside the originally allocated fragment."
  | Memory.StalePointer ->
      format "Stale pointer access" "Tried to access memory that was freed."
  | Memory.OutOfBoundsByteBufferAccess ->
      format "Out of bounds byte buffer access"
        "Byte reads and writes must refer to initialized memory."
  | Memory.UndefinedAccess ->
      format "Access of undefined value"
        "All memory cells must be set before being read."
  | Memory.ZeroLengthAlloc ->
      format "Allocation of length zero"
        "Fragments must have at least one cell."
  | Primitives.IllegalBitShift n ->
      format
        ("Illegal bit shift amount '" ^ Uint64.to_string n ^ "'")
        "Bit counts in bit shifts must be less than 64."
  | Primitives.UnprintableChar c ->
      format
        ("Unprintable character '" ^ Int.to_string (Char.to_int c) ^ "'")
        "Only printable ASCII, spaces, and newline characters are allowed."
  | Runtime.DeferredWord w ->
      format
        ("Undefined deferred word '" ^ w ^ "'")
        "Deferred words must be defined before their first use."
  | Runtime.DuplicateWord w ->
      format
        ("Already defined word '" ^ w ^ "'")
        "Words can only be defined once."
  | Runtime.ExpectedBytes v -> unexpected v "This word required a byte buffer."
  | Runtime.ExpectedInteger v -> unexpected v "This word required a number."
  | Runtime.ExpectedPointer v -> unexpected v "This word required a pointer."
  | Runtime.ExpectedRegion v ->
      unexpected v "This word required a memory region."
  | Runtime.StackUnderflow ->
      format "Stack underflow"
        "Word expected more arguments than were supplied."
  | Runtime.PickUnderflow i ->
      format
        ("Stack underflow when picking " ^ Int.to_string i)
        "There are not enough values in the stack."
  | Runtime.UndeclaredWord w ->
      format
        ("Undeclared word '" ^ w ^ "'")
        "Words defined with ':d' must first be declared with 'defer'."
  | Runtime.UndefinedWord w ->
      format
        ("Undefined word '" ^ w ^ "'")
        "All non-primitive words must be defined before use."
  | Division_by_zero ->
      format "Division by zero" "Integers cannot be divided by zero."
  | e -> raise e

let do_err err env recovered_stack trace =
  let print_val v = Out_channel.eprintf "%s\n" (format_val v) in
  print_cause err ;
  Out_channel.eprintf "%s" "\x1b[1mData stack:\x1b[30m\x1b(B\x1b[m\n" ;
  List.iter recovered_stack ~f:print_val ;
  Out_channel.eprintf "%s" "--------------------\n" ;
  Runtime.iter_stack env ~f:print_val ;
  Out_channel.eprintf "%s" "\n\x1b[1mBacktrace:\x1b[30m\x1b(B\x1b[m\n" ;
  Out_channel.(output_lines stderr (List.rev trace)) ;
  Caml.exit 127

let () =
  let source = In_channel.input_all stdin in
  let ast =
    match Angstrom.parse_string Parser.file source with
    | Ok a -> a
    | Error e -> raise (Failure e) in
  let env = Runtime.Environment.of_primitives Primitives.all in
  Runtime.append_args env ;
  try List.iter ast ~f:(Runtime.eval env) with
  | Runtime.Backtrace (Primitives.StackUnwind (cause, stack), trace) ->
      do_err cause env stack trace
  | Runtime.Backtrace (err, trace) -> do_err err env [] trace
  | err -> do_err err env [] []
