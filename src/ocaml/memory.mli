open Stdint

exception IllegalPath
exception IllegalPointerOffset
exception StalePointer
exception OutOfBoundsByteBufferAccess
exception UndefinedAccess
exception ZeroLengthAlloc

type bytes
type region
type pointer

type value =
  | Int of Uint64.t
  | Bytes of bytes
  | Region of region
  | Pointer of pointer

module Bytes : sig
  val create : unit -> bytes
  val len : bytes -> int
  val clear : bytes -> unit
  val get : bytes -> int -> char
  val set : bytes -> int -> char -> unit
  val push : bytes -> char -> unit
  val append : bytes -> bytes -> unit
end

module Region : sig
  val create : unit -> region
  val alloc : region -> int -> pointer
  val clear : region -> unit
end

module Pointer : sig
  val get : pointer -> value
  val set : pointer -> value -> unit
  val add : pointer -> int -> pointer
end

module File : sig
  val read : bytes -> value
  val write : bytes -> bytes -> bool
end
