exception Backtrace of exn * string list
exception DeferredWord of string
exception DuplicateWord of string
exception ExpectedBytes of Memory.value
exception ExpectedInteger of Memory.value
exception ExpectedPointer of Memory.value
exception ExpectedRegion of Memory.value
exception PickUnderflow of int
exception StackUnderflow
exception UndeclaredWord of string
exception UndefinedWord of string

val truthy : Memory.value -> bool

module Environment : sig
  type t

  val of_primitives : (string * (t -> unit)) list -> t
  val push : t -> Memory.value -> unit
  val pop : t -> Memory.value
  val pick : t -> int -> Memory.value
end

val eval : Environment.t -> Ast.top_level -> unit
val iter_stack : Environment.t -> f:(Memory.value -> unit) -> unit
val append_args : Environment.t -> unit
