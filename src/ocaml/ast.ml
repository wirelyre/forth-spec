open Base

type procedure =
  | Word of string
  | Cond of procedure list * procedure list
  | Loop of procedure list * procedure list
  | Print of string
  | Exit
[@@deriving sexp]

type top_level =
  | TopLevelWord of string
  | Defer of string
  | Definition of string * procedure list
  | DeferredDef of string * procedure list
  | Constant of string
  | Variable of string
[@@deriving sexp_of]
