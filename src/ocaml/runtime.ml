open Base
open Stdint

exception Backtrace of exn * string list
exception DeferredWord of string
exception DuplicateWord of string
exception StackUnderflow
exception PickUnderflow of int
exception UndeclaredWord of string
exception UndefinedWord of string
exception ExpectedBytes of Memory.value
exception ExpectedInteger of Memory.value
exception ExpectedPointer of Memory.value
exception ExpectedRegion of Memory.value
exception EarlyExit (* not exposed in public interface *)

let truthy v =
  match v with Memory.Int i -> Uint64.(compare i zero) = 1 | _ -> true

module Environment = struct
  type t =
    {bindings: (string, action ref) Hashtbl.t; stack: Memory.value Stack.t}

  and action =
    | Primitive of string * (t -> unit)
    | Colon of string * action list
    | Deferred of string
    | Value of Memory.value
    | Word of action ref
    | Cond of action list * action list
    | Loop of action list * action list
    | Print of string
    | Exit

  let of_primitives primitives =
    let bindings =
      List.map
        ~f:(fun (name, prim) -> (name, ref (Primitive (name, prim))))
        primitives in
    { bindings= Hashtbl.of_alist_exn (module String) bindings
    ; stack= Stack.create () }

  let push env v = Stack.push env.stack v

  let pop env =
    match Stack.pop env.stack with Some v -> v | None -> raise StackUnderflow

  let pick env i =
    try (Stack.to_array env.stack).(i) with _ -> raise (PickUnderflow i)

  let set env name b =
    match Hashtbl.add env.bindings ~key:name ~data:b with
    | `Ok -> ()
    | `Duplicate -> raise (DuplicateWord name)

  let get env name =
    match Hashtbl.find env.bindings name with
    | Some b -> b
    | None -> (
      match Parser.number name with
      | Some n -> ref (Value (Int n))
      | None -> raise (UndefinedWord name) )

  let set_value env name v = set env name (ref (Value v))

  let rec resolve env ast =
    let resolve = List.map ~f:(resolve env) in
    match ast with
    | Ast.Word w -> Word (get env w)
    | Ast.Cond (if_true, if_false) -> Cond (resolve if_true, resolve if_false)
    | Ast.Loop (cond, loop) -> Loop (resolve cond, resolve loop)
    | Ast.Print str -> Print str
    | Ast.Exit -> Exit

  let define env name proc =
    let new_word = ref Exit in
    set env name new_word ;
    let resolved = List.map ~f:(resolve env) proc in
    new_word := Colon (name, resolved)

  let defer env name = set env name (ref (Deferred name))

  let define_deferred env name proc =
    match Hashtbl.find env.bindings name with
    | Some bound -> (
      match !bound with
      | Deferred _ ->
          let resolved = List.map ~f:(resolve env) proc in
          bound := Colon (name, resolved)
      | _ -> raise (UndeclaredWord name) )
    | None -> raise (UndeclaredWord name)
end

let backtracing name f x =
  try f x with
  | EarlyExit -> raise EarlyExit
  | Backtrace (cause, trace) -> raise (Backtrace (cause, name :: trace))
  | e -> raise (Backtrace (e, [name]))

let rec run env action =
  let run' = List.iter ~f:(run env) in
  let open Environment in
  match action with
  | Primitive (name, p) -> backtracing name p env
  | Colon (name, c) -> ( try backtracing name run' c with EarlyExit -> () )
  | Deferred name -> raise (DeferredWord name)
  | Value v -> push env v
  | Word w -> run env !w
  | Cond (if_true, if_false) ->
      let value = backtracing "if" pop env in
      let branch = if truthy value then if_true else if_false in
      run' branch
  | Loop (cond, loop) ->
      while
        run' cond ;
        let value = backtracing "while" pop env in
        truthy value
      do
        run' loop
      done
  | Print str -> Stdio.Out_channel.(output_string stderr str ; flush stderr)
  | Exit -> raise EarlyExit

let eval (env : Environment.t) =
  let open Ast in
  function
  | TopLevelWord name -> run env !(Environment.get env name)
  | Defer name -> Environment.defer env name
  | Definition (name, proc) -> Environment.define env name proc
  | DeferredDef (name, proc) -> Environment.define_deferred env name proc
  | Constant name ->
      let value = backtracing "constant" Environment.pop env in
      Environment.set_value env name value
  | Variable name ->
      let region = Memory.Region.create () in
      let pointer = Memory.Region.alloc region 1 in
      Environment.set_value env name (Memory.Pointer pointer)

let iter_stack (env : Environment.t) ~(f : Memory.value -> unit) =
  Stack.iter env.stack ~f

let append_args (env : Environment.t) =
  let argc =
    Uint64.of_int
      (match Array.length (Sys.get_argv ()) with 0 -> 0 | i -> i - 1) in
  let argv =
    let bytes = Memory.Bytes.create () in
    let push_arg arg =
      String.iter ~f:(Memory.Bytes.push bytes) arg ;
      Memory.Bytes.push bytes '\x00' in
    Array.iteri (Sys.get_argv ()) ~f:(fun i arg -> if i > 0 then push_arg arg) ;
    bytes in
  Environment.set_value env "argc" (Memory.Int argc) ;
  Environment.set_value env "argv" (Memory.Bytes argv)
