open Angstrom
open Base

let legal =
  let printable c = Char.('!' <= c && c <= '~') in
  let whitespace c = Char.(c = ' ' || c = '\n') in
  let char_legal c = printable c || whitespace c in
  String.for_all ~f:char_legal

let end_word =
  peek_char
  >>= function
  | Some ' ' | Some '\n' | Some '\\' | None -> return () | _ -> fail ""

let comment = char '\\' <* take_till (fun c -> Char.(c = '\n'))
let whitespace = many (char ' ' <|> char '\n' <|> comment)
let keyword w = whitespace *> string w *> end_word

let name =
  whitespace
  *> take_while1 (fun c ->
         Char.(c <> ' ' && c <> '\n' && c <> '\\' && c <> '"'))
  >>= function
  | ":" | ";" | ":d" | "defer" | "constant" | "variable" | "if" | "else"
   |"then" | "begin" | "while" | "repeat" | "exit" ->
      fail "found reserved word"
  | word -> return word

let print =
  let unescaped =
    take_while1 (fun c -> Char.(c <> '\n' && c <> '\\' && c <> '"')) in
  let escaped =
    char '\\' *> any_char
    >>= function
    | '"' -> return "\""
    | '\\' -> return "\\"
    | 'n' -> return "\n"
    | _ -> fail "unknown escape character" in
  whitespace *> char '"' *> many (unescaped <|> escaped)
  <* commit <* char '"'
  >>| fun parts -> Ast.Print (String.concat parts)

let procedure =
  fix (fun procedure ->
      let cond fst snd = Ast.Cond (fst, snd) in
      let loop while_ do_ = Ast.Loop (while_, do_) in
      many
        (choice
           [ (name >>| fun name -> Ast.Word name)
           ; keyword "if"
             *> lift2 cond procedure (option [] (keyword "else" *> procedure))
             <* commit
             <* (keyword "then" <|> fail "expected then")
           ; keyword "begin"
             *> lift2 loop (procedure <* commit)
                  ((keyword "while" <|> fail "expected while") *> procedure)
             <* commit
             <* (keyword "repeat" <|> fail "expected repeat"); print
           ; keyword "exit" *> return Ast.Exit ]))

let file =
  let definition name contents = Ast.Definition (name, contents) in
  let deferred_def name contents = Ast.DeferredDef (name, contents) in
  many
    (choice
       [ (name >>| fun name -> Ast.TopLevelWord name)
       ; keyword ":" *> commit
         *> lift2 definition name (procedure <* keyword ";")
       ; keyword ":d" *> commit
         *> lift2 deferred_def name (procedure <* keyword ";")
       ; (keyword "defer" *> commit *> name >>| fun name -> Ast.Defer name)
       ; (keyword "constant" *> commit *> name >>| fun name -> Ast.Constant name)
       ; (keyword "variable" *> commit *> name >>| fun name -> Ast.Variable name)
       ])
  <* whitespace <* end_of_input

let number n =
  let parser =
    take_while1 (fun c -> Char.('0' <= c && c <= '9'))
    <* end_of_input >>| Stdint.Uint64.of_string in
  let parsed = parse_string parser n in
  Result.ok parsed
