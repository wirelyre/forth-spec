open Base
open Memory
open Stdint
open Runtime.Environment

exception IllegalBitShift of Uint64.t
exception StackUnwind of exn * Memory.value list
exception UnprintableChar of char

module Conversions = struct
  let uint = function
    | Memory.Int i -> i
    | v -> raise (Runtime.ExpectedInteger v)

  let bytes = function
    | Memory.Bytes b -> b
    | v -> raise (Runtime.ExpectedBytes v)

  let region = function
    | Memory.Region r -> r
    | v -> raise (Runtime.ExpectedRegion v)

  let pointer = function
    | Memory.Pointer p -> p
    | v -> raise (Runtime.ExpectedPointer v)

  let int v = uint v |> Uint64.to_int
  let char v = uint v |> Uint64.to_uint8 |> Uint8.to_int |> Char.of_int_exn
  let of_uint i = Memory.Int i
  let of_char c = Memory.Int (Uint64.of_int (Char.to_int c))
  let of_int i = Memory.Int (Uint64.of_int i)
  let of_bytes b = Memory.Bytes b
  let of_region r = Memory.Region r
  let of_pointer p = Memory.Pointer p
end

open Conversions

type _ lift =
  | Z : Memory.value list lift
  | S : 'a lift -> (Memory.value -> 'a) lift

let rec lift : type a. a lift -> a -> t -> unit = function
  | Z -> fun vals env -> List.iter vals ~f:(push env)
  | S n -> (
      fun f env ->
        let arg = pop env in
        try lift n (f arg) env with
        | StackUnwind (e, args) -> raise (StackUnwind (e, arg :: args))
        | e -> raise (StackUnwind (e, [arg])) )

let lift1 = lift (S Z)
let lift2 = lift (S (S Z))
let lift3 = lift (S (S (S Z)))
let arith1 f = lift1 (fun a -> [of_uint (f (uint a))])
let arith2 f = lift2 (fun b a -> [of_uint (f (uint a) (uint b))])
let push0 f env = push env (f ())
let ignore1 f = lift1 (fun a -> f a ; [])
let ignore2 f = lift2 (fun b a -> f b a ; [])
let ignore3 f = lift3 (fun c b a -> f c b a ; [])
let of_bool b = Uint64.(if b then max_int else min_int)

let shift f a b =
  Uint64.(if compare b (of_int 63) > 0 then raise (IllegalBitShift b)) ;
  f a (Uint64.to_int b)

(** Primitives **)

let putc c =
  if Char.(c <> '\n' && (c < ' ' || '~' < c)) then raise (UnprintableChar c)
  else Stdio.Out_channel.(output_char stderr c ; flush stderr)

let dot i =
  Stdio.(
    eprintf "%s " (Uint64.to_string i) ;
    Out_channel.flush stderr)

let pick env =
  let pos = pop env in
  try
    let pos = uint pos |> Uint64.to_int in
    let value = pick env pos in
    push env value
  with
  | StackUnwind (e, args) -> raise (StackUnwind (e, pos :: args))
  | e -> raise (StackUnwind (e, [pos]))

let arithmetic =
  [ ("+", arith2 Uint64.add); ("-", arith2 Uint64.sub); ("*", arith2 Uint64.mul)
  ; ("/", arith2 Uint64.div)
  ; ("=", arith2 (fun a b -> Uint64.compare a b = 0 |> of_bool))
  ; ("<>", arith2 (fun a b -> Uint64.compare a b <> 0 |> of_bool))
  ; ("<", arith2 (fun a b -> Uint64.compare a b < 0 |> of_bool))
  ; (">", arith2 (fun a b -> Uint64.compare a b > 0 |> of_bool))
  ; ("or", arith2 Uint64.logor); ("xor", arith2 Uint64.logxor)
  ; ("and", arith2 Uint64.logand); ("not", arith1 Uint64.lognot)
  ; ("<<", arith2 (shift Uint64.shift_left))
  ; (">>", arith2 (shift Uint64.shift_right)) ]

let stack_manipulation =
  [ ("drop", lift1 (fun _ -> [])); ("nip", lift2 (fun v _ -> [v]))
  ; ("dup", lift1 (fun v -> [v; v])); ("over", lift2 (fun v2 v1 -> [v1; v2; v1]))
  ; ("tuck", lift2 (fun v2 v1 -> [v2; v1; v2]))
  ; ("swap", lift2 (fun v2 v1 -> [v2; v1]))
  ; ("rot", lift3 (fun v3 v2 v1 -> [v2; v3; v1]))
  ; ("-rot", lift3 (fun v3 v2 v1 -> [v3; v1; v2])); ("pick", pick) ]

let memory_management =
  [ ("rnew", push0 (fun () -> Memory.Region (Memory.Region.create ())))
  ; ("rclear", ignore1 (fun r -> Memory.Region.clear (region r)))
  ; ( "alloc"
    , lift2 (fun r n -> [Memory.Pointer (Region.alloc (region r) (int n))]) )
  ; ("+p", lift2 (fun p n -> [Memory.Pointer (Pointer.add (pointer p) (int n))]))
  ; ("@", lift1 (fun p -> [Pointer.get (pointer p)]))
  ; ("!", ignore2 (fun p v -> Pointer.set (pointer p) v))
  ; ("bnew", push0 (fun () -> Memory.Bytes (Memory.Bytes.create ())))
  ; ("blen", lift1 (fun b -> [Bytes.len (bytes b) |> of_int]))
  ; ("bclear", ignore1 (fun b -> Bytes.clear (bytes b)))
  ; ("b@", lift2 (fun i b -> [Bytes.get (bytes b) (int i) |> of_char]))
  ; ("b!", ignore3 (fun i b c -> Bytes.set (bytes b) (int i) (char c)))
  ; ("b%", ignore2 (fun b c -> Bytes.push (bytes b) (char c)))
  ; ("bapp", ignore2 (fun b2 b1 -> Memory.Bytes.append (bytes b1) (bytes b2)))
  ]

let io =
  [ (".", ignore1 (fun num -> dot (uint num)))
  ; ("fread", lift1 (fun path -> [File.read (bytes path)]))
  ; ( "fwrite"
    , lift2 (fun path data ->
          [Memory.Int (of_bool (File.write (bytes path) (bytes data)))]) )
  ; ("putc", ignore1 (fun c -> putc (char c)))
  ; ("fail", ignore1 (fun code -> Caml.exit (int code))) ]

let all = arithmetic @ stack_manipulation @ memory_management @ io
