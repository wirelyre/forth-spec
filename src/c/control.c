#include "topple.h"
#include "control.h"

static struct control {
    enum CONTROL { IF, ELSE, BEGIN, WHILE, REPEAT } kind;
    size_t place, place2;
} control_stack[16];
static size_t control_stack_top;

void if_(void) {
    code[code_top].kind = BRANCH;
    struct control c = { .kind = IF, .place = code_top };
    control_stack[control_stack_top] = c;

    control_stack_top++;
    code_top++;
}

void else_(void) {
    code[code_top].kind = JUMP;

    size_t if_loc = control_stack[control_stack_top - 1].place;
    code[if_loc].branch = &code[code_top + 1];

    control_stack[control_stack_top - 1].kind = ELSE;
    control_stack[control_stack_top - 1].place = code_top;

    code_top++;
}

void then(void) {
    enum CONTROL kind = control_stack[control_stack_top - 1].kind;
    size_t place = control_stack[control_stack_top - 1].place;

    if (kind == IF) code[place].branch = &code[code_top];
    else            code[place].jump   = &code[code_top];

    control_stack_top--;
}

void begin(void) {
    struct control c = { .kind = BEGIN, .place = code_top };
    control_stack[control_stack_top] = c;
    control_stack_top++;
}

void while_(void) {
    code[code_top].kind = BRANCH;

    control_stack[control_stack_top - 1].place2 = code_top;

    code_top++;
}

void repeat(void) {
    struct control c = control_stack[control_stack_top - 1];

    code[code_top].kind = JUMP;
    code[code_top].jump = &code[c.place];

    code[c.place2].branch = &code[code_top + 1];

    control_stack_top--;
    code_top++;
}
