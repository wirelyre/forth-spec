#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "topple.h"



static char getescape() {
    switch (getchar()) {
        case 'n':  return '\n';
        case '\\': return '\\';
        case '"':  return '"';
    }
    return '\0';
}

char *read_word() {
    size_t i = 0;
    int c;
    size_t max = 16;
    char *buf = malloc(max);

    while ((c = getchar()) != EOF) {
        if (i + 1 >= max) {
            max *= 2;
            buf = realloc(buf, max);
        }

        switch (c) {

        case '"':
            i = 0;
            buf[i++] = '"';
            while ((c = getchar()) != '"') {
                if (c == '\\')
                    buf[i++] = getescape();
                else
                    buf[i++] = c;
            }
            buf[i] = 0;
            return buf;

        case '\\':
            while (true) {
                c = getchar();
                if (c == '\r' || c == '\n')
                    break;
            }
        // FALL THROUGH!
        case ' ':
        case '\r':
        case '\n':
            if (i > 0) {
                buf[i] = 0;
                return buf;
            }
            break;

        default:
            buf[i++] = c;
            break;

        }
    }

    return NULL;
}

struct operation *find_operation(const char *name) {
    struct entry *e = dictionary;
    while (e != NULL) {
        if (strcmp(e->name, name) == 0) return e->code;
        e = e->next;
    }
    return NULL;
}

struct primitive *find_primitive(const char *name) {
    struct primitive *p = primitives;
    while (p->name != NULL) {
        if (strcmp(p->name, name) == 0) return p;
        p++;
    }
    return NULL;
}



#define S (*s)

static void run(struct operation *start, stack *s) {
    struct operation *return_stack[1024];
    size_t i = 0;

    return_stack[i++] = start;

    while (i > 0) {
        struct operation *top = return_stack[i-1];
        return_stack[i-1]++;

        switch (top->kind) {

        case PUSH:
            S++[0] = top->push;
            break;

        case CALL:
            return_stack[i++] = top->call;
            break;

        case RETURN:
            i--;
            break;

        case JUMP:
            return_stack[i-1] = top->jump;
            break;

        case BRANCH:
            if (S--[-1].num == 0)
                return_stack[i-1] = top->branch;
            break;

        case PRIMITIVE:
            top->primitive(s);
            break;

        case STRING:
            fputs(top->string, stderr);
            break;

        }
    }
}

static void do_word(const char *name, stack *s) {
    struct operation *o = find_operation(name);
    if (o != NULL) {
        if (compiling) {
            code[code_top].kind = CALL;
            code[code_top].call = o;
            code_top++;
        }
        else run(o, s);
        return;
    }

    struct primitive *p = find_primitive(name);
    if (p != NULL) {
        if (compiling) {
            if (p->compile != NULL) p->compile();
            else {
                code[code_top].kind = PRIMITIVE;
                code[code_top].primitive = p->interpret;
                code_top++;
            }
        } else p->interpret(s);
        return;
    }

    uint64_t num = 0;
    size_t i = 0;
    while (true) {
        if (name[i] == '\0') {
            if (compiling) {
                code[code_top].kind = PUSH;
                code[code_top].push.num = num;
                code_top++;
            } else {
                S++[0].num = num;
            }
            return;
        } else if (name[i] >= '0' && name[i] <= '9') {
            num *= 10;
            num += name[i] - '0';
        } else {
            break;
        }
        i++;
    }
}

static void do_string(const char *buf) {
    if (compiling) {
        char *s = malloc(strlen(buf));
        strcpy(s, buf);
        code[code_top].kind = STRING;
        code[code_top].string = s;
        code_top++;
    } else {
        // TODO: not in OCaml specification
        fprintf(stderr, "%s", buf);
    }
}



void dump_code() {
    for (size_t i = 0; i < code_top; i++) {
        switch (code[i].kind) {
        case PUSH: printf("PUSH 0x%"PRIx64"\n", code[i].push.num); break;
        case CALL: printf("CALL %li\n", code[i].call - code); break;
        case RETURN: printf("RETURN\n"); break;
        case JUMP: printf("JUMP %li\n", code[i].jump - code); break;
        case BRANCH: printf("BRANCH %li\n", code[i].branch - code); break;
        case PRIMITIVE: printf("PRIMITIVE\n"); break;
        case STRING: printf("STRING \"%s\"\n", code[i].string); break;
        }
    }
}

int main(int argc, const char *argv[]) {
    set_up_args(argc, argv);

    char *name;
    stack s = malloc(sizeof(union value) * 1024);

    while ((name = read_word())) {
        if (name[0] == '"') do_string(name+1);
        else do_word(name, &s);

        free(name);
    }

    return 0;
}
