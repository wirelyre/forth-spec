#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "topple.h"
#include "control.h"
#include "memory.h"



#define S (*s)
#define ARITH(name, expr) static void name(stack *s) { \
    uint64_t lhs = S[-2].num, rhs = S[-1].num; \
    S[-2].num = (expr); \
    S--; \
}
#define BOOL(name, expr) ARITH(name, (expr) ? (uint64_t) -1 : 0)

ARITH (add,  lhs +  rhs)
ARITH (sub,  lhs -  rhs)
ARITH (mul,  lhs *  rhs)
ARITH (div_, lhs /  rhs)
BOOL  (eq,   lhs == rhs)
BOOL  (neq,  lhs != rhs)
BOOL  (lt,   lhs <  rhs)
BOOL  (gt,   lhs >  rhs)
ARITH (or,   lhs |  rhs)
ARITH (xor,  lhs ^  rhs)
ARITH (and,  lhs &  rhs)
ARITH (shl,  lhs << rhs)
ARITH (shr,  lhs >> rhs)
static void not(stack *s) { S[-1].num = ~S[-1].num; }

static void drop (stack *s) { S--; }
static void nip  (stack *s) { S[-2] = S[-1]; S--; }
static void dup  (stack *s) { S++; S[-1] = S[-2]; }
static void over (stack *s) { S++; S[-1] = S[-3]; }
static void tuck (stack *s) { S++; S[-1] = S[-2]; S[-2] = S[-3]; S[-3] = S[-1]; }
static void swap (stack *s) { union value a = S[-1]; S[-1] = S[-2]; S[-2] = a; }
static void rot(stack *s) {
    union value a = S[-1];
    S[-1] = S[-3];
    S[-3] = S[-2];
    S[-2] = a;
}
static void rot_(stack *s) {
    union value a = S[-1];
    S[-1] = S[-2];
    S[-2] = S[-3];
    S[-3] = a;
}
static void pick(stack *s) {
    union value v = S[-1];
    S[-1] = S[-(2 + v.num)];
}

static void dot  (stack *s) { fprintf(stderr, "%"PRIu64" ", S[-1].num); S--; }
static void putc_(stack *s) { fputc((uint8_t) S[-1].num, stderr); S--; }
static void fail (stack *s) { exit(S[-1].num); }



static void link_name(char *const name) {
    struct entry *e = malloc(sizeof(struct entry) + strlen(name));
    e->code = &code[code_top];
    e->next = dictionary;
    strcpy(e->name, name);
    dictionary = e;

    free(name);
}

static void colon(stack *s) {
    link_name(read_word());
    compiling = true;
}

static void semicolon(void) {
    code[code_top++].kind = RETURN;
    compiling = false;
}

static void exit_(void) {
    code[code_top++].kind = RETURN;
}

static void defer(stack *s) {
    link_name(read_word());
    code[code_top++].kind = RETURN;
}

static void colon_d(stack *s) {
    char *const name = read_word();

    struct operation *op = find_operation(name);
    op->kind = JUMP;
    op->jump = &code[code_top];

    link_name(name);
    compiling = true;
}



static void constant(stack *s) {
    link_name(read_word());

    code[code_top].kind = PUSH;
    code[code_top].push = S[-1];
    code_top++;
    S--;

    code[code_top++].kind = RETURN;
}

static void variable(stack *s) {
    link_name(read_word());

    code[code_top].kind = PUSH;
    code[code_top].push.ptr = malloc(sizeof(union value));
    code_top++;

    code[code_top++].kind = RETURN;
}



static struct primitive primitives_[] = {
    { .name = "+",        .interpret = add      },
    { .name = "-",        .interpret = sub      },
    { .name = "*",        .interpret = mul      },
    { .name = "/",        .interpret = div_     },
    { .name = "=",        .interpret = eq       },
    { .name = "<>",       .interpret = neq      },
    { .name = "<",        .interpret = lt       },
    { .name = ">",        .interpret = gt       },
    { .name = "or",       .interpret = or       },
    { .name = "xor",      .interpret = xor      },
    { .name = "and",      .interpret = and      },
    { .name = "not",      .interpret = not      },
    { .name = "<<",       .interpret = shl      },
    { .name = ">>",       .interpret = shr      },
    { .name = "drop",     .interpret = drop     },
    { .name = "nip",      .interpret = nip      },
    { .name = "dup",      .interpret = dup      },
    { .name = "over",     .interpret = over     },
    { .name = "tuck",     .interpret = tuck     },
    { .name = "swap",     .interpret = swap     },
    { .name = "rot",      .interpret = rot      },
    { .name = "-rot",     .interpret = rot_     },
    { .name = "pick",     .interpret = pick     },
    { .name = ".",        .interpret = dot      },
    { .name = "rnew",     .interpret = rnew     },
    { .name = "rclear",   .interpret = rclear   },
    { .name = "alloc",    .interpret = alloc    },
    { .name = "+p",       .interpret = addp     },
    { .name = "@",        .interpret = get      },
    { .name = "!",        .interpret = set      },
    { .name = "bnew",     .interpret = bnew     },
    { .name = "blen",     .interpret = blen     },
    { .name = "bclear",   .interpret = bclear   },
    { .name = "b@",       .interpret = bget     },
    { .name = "b!",       .interpret = bset     },
    { .name = "b%",       .interpret = bpush    },
    { .name = "bapp",     .interpret = bapp     },
    { .name = "fread",    .interpret = fread_   },
    { .name = "fwrite",   .interpret = fwrite_  },
    { .name = "putc",     .interpret = putc_    },
    { .name = "fail",     .interpret = fail     },
    { .name = "argc",     .interpret = argc     },
    { .name = "argv",     .interpret = argv     },
    { .name = ":",        .interpret = colon    },
    { .name = ";",        .compile = semicolon  },
    { .name = "exit",     .compile = exit_      },
    { .name = "if",       .compile = if_        },
    { .name = "else",     .compile = else_      },
    { .name = "then",     .compile = then       },
    { .name = "begin",    .compile = begin      },
    { .name = "while",    .compile = while_     },
    { .name = "repeat",   .compile = repeat     },
    { .name = "defer",    .interpret = defer    },
    { .name = ":d",       .interpret = colon_d  },
    { .name = "constant", .interpret = constant },
    { .name = "variable", .interpret = variable },
    { .name = NULL                              },
};
struct primitive *primitives = primitives_;
