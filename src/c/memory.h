#include "topple.h"

void rnew   (stack *);
void rclear (stack *);
void alloc  (stack *);
void addp   (stack *);
void get    (stack *);
void set    (stack *);

void bnew   (stack *);
void blen   (stack *);
void bclear (stack *);
void bget   (stack *);
void bset   (stack *);
void bpush  (stack *);
void bapp   (stack *);

void fread_ (stack *);
void fwrite_(stack *);

void argc   (stack *);
void argv   (stack *);
