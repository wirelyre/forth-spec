#ifndef TOPPLE_H
#define TOPPLE_H



#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>



struct bytebuf;
struct entry;
struct operation;
struct primitive;
struct region;
union value;

struct operation *find_operation(const char *name);
struct primitive *find_primitive(const char *name);
char *read_word();

bool compiling;
struct entry *dictionary;
struct primitive *primitives;

void set_up_args(int argc, const char *argv[]);



union value {
    uint64_t num;
    union value *ptr;
    struct region *region;
    struct bytebuf *bytes;
};

typedef union value *stack;



struct operation {
    enum {PUSH, CALL, RETURN, JUMP, BRANCH, PRIMITIVE, STRING} kind;
    union {
        union value push;
        struct operation *call;
        struct operation *jump;
        struct operation *branch;
        void (*primitive)(stack *);
        const char *string;
    };
};

struct operation code[1024 * 1024];
size_t code_top;



struct entry {
    struct operation *code;
    struct entry *next;
    char name[];
};

struct primitive {
    char *name;
    void (*compile)(void);
    void (*interpret)(stack *);
};



#endif
