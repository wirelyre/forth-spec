#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "topple.h"
#include "memory.h"



struct fragment {
    size_t pos;
    size_t cap;
    struct fragment *prev;
    union value contents[];
};

struct region {
    struct fragment *last;
};

#define S (*s)

void rnew(stack *s) {
    S += 1;
    S[-1].region = malloc(sizeof(struct region));
    S[-1].region->last = NULL;
}

void rclear(stack *s) {
    struct region *r = S--[-1].region;

    struct fragment *this = r->last,
                    *next;

    while (this) {
        next = this->prev;
        free(this);
        this = next;
    }

    r->last = NULL;
}

void alloc(stack *s) {
    struct region *r = S[-1].region;
    uint64_t size = S[-2].num;
    S -= 1;

    struct fragment *into = r->last;

    if (into == NULL || into->pos + size >= into->cap) {
        size_t cap = 1020;
        if (cap < size * 16) cap = size * 16;
        if (into != NULL && into->cap > cap) cap = into->cap;

        into = malloc(sizeof(struct fragment) + cap*sizeof(union value));
        into->pos = 0;
        into->cap = cap;
        into->prev = r->last;

        r->last = into;
    }

    S[-1].ptr = &into->contents[into->pos];

    into->pos += size;
}

void addp(stack *s) {
    union value *ptr = S[-1].ptr;
    int64_t offset = S[-2].num; // unsigned to signed cast
    S -= 1;

    S[-1].ptr = ptr + offset;
}

void get(stack *s) {
    S[-1] = *S[-1].ptr;
}

void set(stack *s) {
    *S[-1].ptr = S[-2];
    S -= 2;
}



struct bytebuf {
    size_t pos;
    size_t cap;
    char *contents;
};

void bnew(stack *s) {
    S += 1;
    S[-1].bytes = malloc(sizeof(struct bytebuf));

    S[-1].bytes->pos = 0;
    S[-1].bytes->cap = 4096;
    S[-1].bytes->contents = malloc(4096);
}

void blen(stack *s) {
    S[-1].num = S[-1].bytes->pos;
}

void bclear(stack *s) {
    struct bytebuf *buf = S[-1].bytes;
    S -= 1;

    buf->pos = 0;
    buf->cap = 4096;
    buf->contents = realloc(buf->contents, 4096);
}

void bget(stack *s) {
    uint64_t        idx = S[-1].num;
    struct bytebuf *buf = S[-2].bytes;
    S -= 1;

    S[-1].num = (uint8_t) buf->contents[idx];
}

void bset(stack *s) {
    uint64_t        idx = S[-1].num;
    struct bytebuf *buf = S[-2].bytes;
    uint8_t         val = S[-3].num; // truncate
    S -= 3;

    buf->contents[idx] = val;
}

void bpush(stack *s) {
    struct bytebuf *buf = S[-1].bytes;
    uint8_t         val = S[-2].num; // truncate
    S -= 2;

    if (buf->pos >= buf->cap) {
        buf->cap = buf->cap * 2;
        buf->contents = realloc(buf->contents, buf->cap);
    }

    buf->contents[buf->pos] = val;
    buf->pos += 1;
}

void bapp(stack *s) {
    struct bytebuf *b2 = S[-1].bytes,
                   *b1 = S[-2].bytes;
    S -= 2;

    while (b1->pos + b2->pos > b1->cap) {
        b1->cap = b1->cap * 2;
    }
    b1->contents = realloc(b1->contents, b1->cap);

    memcpy(&b1->contents[b1->pos], b2->contents, b2->pos);
    b1->pos += b2->pos;
}



static FILE *open_path(stack *s, const char *mode) {
    // zero terminate
    S += 2;
    S[-2].num = 0;
    S[-1] = S[-3];
    bpush(s);

    struct bytebuf *path = S[-1].bytes;
    FILE *file = fopen(path->contents, mode);

    // un-zero terminate
    path->pos -= 1;

    return file;
}

// TODO: each file open/close leaks two allocations

void fread_(stack *s) {
    FILE *file = open_path(s, "rb");

    if (file == NULL) {
        S[-1].num = 0;
        return;
    }

    size_t len;
    fseek(file, 0, SEEK_END);
    len = ftell(file);
    fseek(file, 0, SEEK_SET);

    char *bytes = malloc(len);
    fread(bytes, 1, len, file);

    fclose(file);

    S[-1].bytes = malloc(sizeof(struct bytebuf));
    S[-1].bytes->pos = len;
    S[-1].bytes->cap = len;
    S[-1].bytes->contents = bytes;
}

void fwrite_(stack *s) {
    FILE *file = open_path(s, "rb");
    fclose(file);

    if (file) { // file exists
        S -= 1;
        S[-1].num = 0;
        return;
    }

    file = open_path(s, "wb");
    struct bytebuf *data = S[-2].bytes;
    S -= 1;

    size_t written = fwrite(data->contents, 1, data->pos, file);
    fclose(file);

    if (written < data->pos)
        S[-1].num = 0;
    else
        S[-1].num = (uint64_t) (int64_t) -1;
}



static int argc_;
static struct bytebuf *argv_;

void argc(stack *s) {
    S++;
    S[-1].num = argc_;
}

void argv(stack *s) {
    S++;
    S[-1].bytes = argv_;
}

void set_up_args(int argc, const char *argv[]) {
    argc_ = argc - 1;

    size_t len = 0;
    for (size_t i = 1; i < argc; i++)
        len += strlen(argv[i]) + 1;

    char *bytes = malloc(len);
    argv_ = malloc(sizeof(struct bytebuf));
    argv_->pos = len;
    argv_->cap = len;
    argv_->contents = bytes;

    for (size_t i = 1; i < argc; i++) {
        for (size_t j = 0; argv[i][j] != 0; j++) {
            *bytes = argv[i][j];
            bytes += 1;
        }
        *bytes = 0;
        bytes += 1;
    }
}
