#!/usr/bin/env python3


from pathlib import Path
import subprocess
import sys


# Find test files

test_dir = Path(__file__).parent

test_files = list(test_dir.glob("unit/**/*.tpl"))
test_files.sort()


# Global variables

had_error = False

passed = 0
failed = 0
skipped = 0


def bold(message):
    return "\033[1m{}\033[0m".format(message)


def error(header, file, message):
    global had_error
    print("{:>7} {}: {}".format("[" + header + "]", bold(file), message))
    had_error = True


def run_test(language, interpreter, file, expected_output):
    """Run a single test file using the given interpreter.

    Reports an error if the interpreter fails or times out, or if the expected
    output is not observed.

    Returns True if interpretation completed successfully, regardless of the
    observed output; returns False if it failed or timed out.
    """

    global passed, failed

    executable = "build/" + interpreter

    try:
        result = subprocess.run(
            executable,
            stdin=file.open(),
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
            timeout=5,
            check=True,
        )
    except subprocess.TimeoutExpired:
        error(language, file, "timed out")
        failed += 1
        return False
    except subprocess.CalledProcessError as result:
        err = result.stderr.decode("ascii", errors="replace")
        msg = "abnormal exit; captured stderr:\n{}".format(err)
        error(language, file, msg)
        failed += 1
        return False

    if result.stderr == expected_output:
        passed += 1
    else:
        msg = "expected {}, found {}".format(expected_output, result.stderr)
        error(language, file, msg)
        failed += 1

    return True


# Run tests

for i, file in enumerate(test_files):
    print("[{}/{}]".format(i, len(test_files)), end="\r")

    with file.open("rb") as f:
        first_line = f.readline()

    if first_line[:10] != b"\\ OUTPUT: ":
        error("ERROR", file, "no expected output found")
        continue

    expected_output = first_line[10:-1]

    # If OCaml fails, skip the others. There might be undefined behavior.
    if not run_test("OCaml", "tpl-ocaml", file, expected_output):
        skipped += 2
        continue
    run_test("C", "tpl-c", file, expected_output)
    run_test("ASM", "tpl-asm", file, expected_output)


# Report test results

print(bold("{:>4} passed".format(passed)))
print(bold("{:>4} failed".format(failed)))
print(bold("{:>4} skipped".format(skipped)))


if had_error:
    sys.exit(1)
