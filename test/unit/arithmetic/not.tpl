\ OUTPUT: 0 1 1 1 

: =. = 0 swap - . ; \ prints 1 if equal, 0 if not equal
: .b 0 = 1 + . ;    \ prints 1 if truthy (if not zero), or 0 if falsy

1 2 < not .b \ 0
2 2 < not .b \ 1
3 2 < not .b \ 1

: 0xAAAAAAAAAAAAAAAA 12297829382473034410 ;
: 0x5555555555555555 6148914691236517205 ;

0xAAAAAAAAAAAAAAAA not 0x5555555555555555 =. \ 1
