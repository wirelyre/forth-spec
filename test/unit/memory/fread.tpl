\ OUTPUT: 471 92 0 1 

bnew constant real-file
bnew constant fake-file

: push-to-buffer
  begin over while tuck b% repeat
  drop drop
;

0 108 112 116 46 100 97 101 114 102 47 121 114 111 109 101 109 47 116 105 110
117 47 116 115 101 116 \ test/unit/memory/fread.tpl
real-file push-to-buffer

0 108 112 116 46 101 108 105 102 45 101 107 97 102 \ fake-file.tpl
fake-file push-to-buffer

1

real-file fread
dup blen .
0 b@ . \ 92 (backslash)

fake-file fread . \ 0

. \ 1
