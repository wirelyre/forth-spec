\ OUTPUT: 1 0 + - 

: print-nonzero
  dup if dup . then
  drop
;

0 print-nonzero \
1 print-nonzero \ 1

: max_signed 9223372036854775808 ;
: print-sign
  dup max_signed < if
    dup if
      43 putc \ '+'
    else
      48 putc \ '0'
    then
  else
    45 putc \ '-'
  then
  32 putc \ ' '
  drop
;

: -2 0 2 - ;

0 print-sign \ 0
2 print-sign \ +
-2 print-sign \ -
