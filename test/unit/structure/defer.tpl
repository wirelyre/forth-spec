\ OUTPUT: 1 2 3 0 

defer do-1.
defer do-2
defer do-.

0
: a do-1.     ;
: b do-2 .    ;
: c 3    do-. ;

:d do-1. 1 . ;
:d do-2  2   ;
:d do-.    . ;

a \ 1
b \ 2
c \ 3
. \ 0
