\ OUTPUT: 1 3 4 

: a   1 .   exit   2 .   ;

a \ 1

: print-if-nonzero dup if . exit then drop ;

0 print-if-nonzero
3 print-if-nonzero \ 3

: b
  begin 1 while
    4 .
    exit
  repeat
;

b \ 4
