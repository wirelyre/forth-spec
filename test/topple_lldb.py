"""
Scripts for inspecting the data structures of a running `tpl-asm` process.

Load this file with:
(lldb) command script import test/topple_lldb.py
"""


from collections import OrderedDict
import lldb


def __lldb_init_module(debugger, internal_dict):
    debugger.HandleCommand(
        "command script add -f topple_lldb.list_words list_words"
    )
    debugger.HandleCommand(
        "command script add -f topple_lldb.decompile_word decompile_word"
    )


def register(process, register):
    """Access the unsigned numeric value of a particular register."""
    thread = process.GetSelectedThread()
    registers = next(iter(thread)).registers[0]
    value = registers.GetChildMemberWithName(register)
    return value.GetValueAsUnsigned()


class Dictionary:
    """The list of currently-defined words."""

    def __init__(self, process, err):
        self.words = OrderedDict()

        # Navigate through linked list

        entry = register(process, "r14")
        next = None

        while entry:
            name = process.ReadCStringFromMemory(entry, 23, err)
            immediate = process.ReadUnsignedFromMemory(entry + 23, 1, err) != 0

            if name not in self.words:
                self.words[name] = {
                    "name": name,
                    "addr": entry,
                    "next": next,
                    "immediate": immediate,
                }

            next = entry
            entry = process.ReadPointerFromMemory(entry + 32, err)

    def __iter__(self):
        return (self.words[word] for word in reversed(self.words))

    def __contains__(self, word):
        return word in self.words

    def __getitem__(self, word):
        return self.words[word]

    def is_empty(self):
        return len(self.words) == 0

    def get_from_address(self, addr):
        for word in self:
            if word["addr"] == addr:
                return word


def list_words(debugger, command, result, internal_dict):
    """
    Print the list of all currently-defined words.

    Run this as an LLDB script:
    (lldb) list_words
    """

    target = debugger.GetSelectedTarget()
    process = target.GetProcess()
    thread = process.GetSelectedThread()

    err = lldb.SBError()
    dictionary = Dictionary(process, err)

    for entry in dictionary:
        result.write(
            "    0x{:x}: {:8}{}\n".format(
                entry["addr"],
                entry["name"],
                "  (IMM)" if entry["immediate"] else "",
            )
        )


def resolve_symbol(target, addr):
    """Try to find a symbol corresponding to the given address."""
    addr = lldb.SBAddress(addr, target)
    return addr.GetSymbol().name or str(addr)


def decompile_word(debugger, command, result, internal_dict):
    """
    Print the instructions in a given word. This includes regular words like
    `+` , and special instructions like `BRANCH`.

    Run this as an LLDB script:
    (lldb) decompile_word
    (lldb) decompile_word a

    If no word is supplied to this script, it will use the last word defined.
    """

    target = debugger.GetSelectedTarget()
    process = target.GetProcess()
    thread = process.GetSelectedThread()

    err = lldb.SBError()
    dictionary = Dictionary(process, err)

    if dictionary.is_empty():
        result.write("there are no words in the dictionary\n")
        return

    if command == "":
        last_word = register(process, "r14")
        word = dictionary.get_from_address(last_word)
    elif command in dictionary:
        name = command
        word = dictionary[name]
    else:
        word = None

    if not word:
        result.write("word '{}' is not in the dictionary\n".format(command))
        return

    # Print header

    addr = process.ReadPointerFromMemory(word["addr"] + 24, err)

    result.write(
        "{}  ({}){}\n".format(
            word["name"],
            resolve_symbol(target, addr),
            " (IMM)" if word["immediate"] else "",
        )
    )

    # Print contents

    start = word["addr"] + 40
    end = word["next"] or register(process, "r13")

    for addr in range(start, end, 8):
        result.write("    0x{:x}:  ".format(addr))

        value = process.ReadPointerFromMemory(addr, err)
        maybe_word = dictionary.get_from_address(value - 24)

        if maybe_word:
            # Padded to 16 characters, but can be up to 23 long.
            result.write(
                "{:16}  0x{:0>8x}\n".format(maybe_word["name"], value - 24)
            )
        else:
            result.write("{0:<16}  0x{0:0>8x}\n".format(value))
